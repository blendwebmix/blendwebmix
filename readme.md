#[Installation wordpress avec Theme BlendWebMix 2020]

Ce theme est basé sur Sage 

## Tips de fonctionnement
Les boutons du header dépendent de noms de page :

- programme
- deviens-sponsor
- ticketing

La présence de ces noms de slug de page ait apparaitre ou disparaire le bouton

## Bas de page
La partie contact est un formulaire sendinblue

## Pour les devs

* Gitlab : https://gitlab.com/blendwebmix/blendwebmix

C'est une installation complète du site sans les images (uploads dans wp-content)

* Utilisation de docker

A la racine, il y a un fichier docker-compose-sample.yml

Il y a 3 containers : 
- wordpress
- mysql
- phpmyadmin

Il faut créer un repertoire data pour les données de Mysql

Ensuite pour lancer 

```shell
docker-compose -f C:\docker\www\blendwebmix\docker-compose.yml up -d
```

Pour arreter

```shell script
docker-compose -f C:\docker\www\blendwebmix\docker-compose.yml down
```

