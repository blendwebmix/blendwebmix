��    %      D  5   l      @     A     I     Z  	   j     t     �     �  	   �     �     �     �     �     �     �               #     4     <     E     V     i     {     �     �     �     �     �     �     �     �       	             !     )  �  /          *      I     j     |  	   �     �     �     �     �     �     �     �  *    	     +	     @	     F	     f	  	   r	     |	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	  
   �	     �	     �	     
     
  	   
     
     
       "                                          $                                    #         %              	                                                        !          Add new Add new attendee Add new session All Rooms Archive page slugspeakers Archive page slugteams Attendee Attendees Day Edit attendee Jobs New attendee No attendees found No attendees found in Trash Parent attendee: Room Search attendees Session Sessions Sessions dayDay Sessions daysDays Sessions typeJob Sessions typeRoom Sessions typesJobs Sessions typesRooms Single page slugspeakers Single page slugteams Speaker Speakers Team Teams View attendee attendees days program rooms Project-Id-Version: Session management
POT-Creation-Date: 2018-06-27 11:35+0200
PO-Revision-Date: 2018-06-27 11:36+0200
Last-Translator: Florian Truchot <florian@fantassin.fr>
Language-Team: Florian Truchot <florian@fantassin.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-WPHeader: bwm-session.php
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Ajouter une nouvelle Ajouter un nouveau participant Ajouter une nouvelle conférence Toutes les salles conferenciers benevoles Participant Participants Jours Modifier le participant Pôles Nouveau participant Aucun participant trouvé Aucun participant trouvé dans la poubelle Participant parent : Salle Chercher parmi les participants Conférence Programme Jours Jours Pôle Salle Pôles Salles conferenciers benevoles Conférencier Conférenciers Bénévole Bénévoles Voir le participant participants jours programme salles 