<?php

/*
 * Plugin Name: Sponsor management
 * Description: Allow sponsor management in WordPress
 * Version: 1.1.0
 * Author: Florian Truchot
 * Author URI: http://www.truchot.co
 * Text Domain: bwm
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


if ( ! function_exists( 'bwm_i18n_setup' ) ) {

	/**
	 * Load plugin textdomain.
	 *
	 * @since 1.0.0
	 */
	function bwm_i18n_setup() {
		load_muplugin_textdomain( 'bwm', 'languages' );
	}

	add_action( 'plugins_loaded', 'bwm_i18n_setup' );
}


/**
 * Registers a new post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string Sponsors
 * @param array|string See optional args description above.
 *
 * @return object|WP_Error the registered post type object, or an error object
 */
function bwm_register_sponsor_post_type() {

	$labels = array(
		'name'               => __( 'Sponsors', 'bwm' ),
		'singular_name'      => __( 'Sponsor', 'bwm' ),
		'add_new'            => __( 'Add new', 'bwm' ),
		'add_new_item'       => __( 'Add new sponsor', 'bwm' ),
		'edit_item'          => __( 'Edit sponsor', 'bwm' ),
		'new_item'           => __( 'New sponsor', 'bwm' ),
		'view_item'          => __( 'View sponsor', 'bwm' ),
		'search_items'       => __( 'Search sponsors', 'bwm' ),
		'not_found'          => __( 'No sponsors found', 'bwm' ),
		'not_found_in_trash' => __( 'No sponsors found in Trash', 'bwm' ),
		'parent_item_colon'  => __( 'Parent sponsor:', 'bwm' ),
		'menu_name'          => __( 'Sponsors', 'bwm' ),
	);


	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_rest'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-nametag',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => __( 'sponsors', 'bwm' ),
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array(
			'slug'       => __( 'sponsors', 'bwm' ),
			'with_front' => false
		),
		'capability_type'     => 'post',
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' )
	);

	register_post_type( 'sponsor', $args );

}

add_action( 'init', 'bwm_register_sponsor_post_type' );

/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 *
 * @return null|WP_Error WP_Error if errors, otherwise null.
 */
function bwm_register_sponsor_type_taxonomy() {

	$labels = array(
		'name'                  => _x( 'Types', 'Sponsors types', 'bwm' ),
		'singular_name'         => _x( 'Type', 'Sponsors type', 'bwm' ),
		'search_items'          => __( 'Search Types', 'bwm' ),
		'popular_items'         => __( 'Popular Types', 'bwm' ),
		'all_items'             => __( 'All Types', 'bwm' ),
		'parent_item'           => __( 'Parent Type', 'bwm' ),
		'parent_item_colon'     => __( 'Parent Type', 'bwm' ),
		'edit_item'             => __( 'Edit Type', 'bwm' ),
		'update_item'           => __( 'Update Type', 'bwm' ),
		'add_new_item'          => __( 'Add New Type', 'bwm' ),
		'new_item_name'         => __( 'New Type Name', 'bwm' ),
		'add_or_remove_items'   => __( 'Add or remove Types', 'bwm' ),
		'choose_from_most_used' => __( 'Choose from most used Types', 'bwm' ),
		'menu_name'             => __( 'Type', 'bwm' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_ui'           => true,
		'show_in_menu'      => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'query_var'         => true,
		'rewrite'           => true,
//		'capabilities'      => array(),
	);

	register_taxonomy( 'sponsor_type', array( 'sponsor' ), $args );
}

add_action( 'init', 'bwm_register_sponsor_type_taxonomy' );
