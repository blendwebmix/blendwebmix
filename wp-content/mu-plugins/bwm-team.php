<?php

/*
 * Plugin Name: Team management
 * Description: Allow team management in WordPress
 * Version: 1.0.0
 * Author: Florian Truchot
 * Author URI: http://www.truchot.co
 * Text Domain: bwm
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


if ( ! function_exists( 'bwm_i18n_setup' ) ) {

	/**
	 * Load plugin textdomain.
	 *
	 * @since 1.0.0
	 */
	function bwm_i18n_setup() {
		load_muplugin_textdomain( 'bwm', 'languages' );
	}

	add_action( 'plugins_loaded', 'bwm_i18n_setup' );
}


/**
 * Registers a new post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string Teams
 * @param array|string See optional args description above.
 *
 * @return object|WP_Error the registered post type object, or an error object
 */
function bwm_register_team_post_type() {

	$labels = array(
		'name'               => __( 'Teams', 'bwm' ),
		'singular_name'      => __( 'Team', 'bwm' ),
		'add_new'            => __( 'Add new', 'bwm' ),
		'add_new_item'       => __( 'Add new team', 'bwm' ),
		'edit_item'          => __( 'Edit team', 'bwm' ),
		'new_item'           => __( 'New team', 'bwm' ),
		'view_item'          => __( 'View team', 'bwm' ),
		'search_items'       => __( 'Search teams', 'bwm' ),
		'not_found'          => __( 'No teams found', 'bwm' ),
		'not_found_in_trash' => __( 'No teams found in Trash', 'bwm' ),
		'parent_item_colon'  => __( 'Parent team:', 'bwm' ),
		'menu_name'          => __( 'Teams', 'bwm' ),
	);


	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_rest'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-groups',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => _x( 'teams', 'Archive page slug', 'bwm' ),
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array(
			'slug'       => _x( 'teams', 'Single page slug', 'bwm' ),
			'with_front' => false
		),
		'capability_type'     => 'post',
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' )
	);

	register_post_type( 'team', $args );

}

add_action( 'init', 'bwm_register_team_post_type' );


/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 *
 * @return null|WP_Error WP_Error if errors, otherwise null.
 */
function bwm_register_team_job_taxonomy() {

	$labels = array(
		'name'                  => _x( 'Jobs', 'Sessions types', 'bwm' ),
		'singular_name'         => _x( 'Job', 'Sessions type', 'bwm' ),
		'search_items'          => __( 'Search Jobs', 'bwm' ),
		'popular_items'         => __( 'Popular Jobs', 'bwm' ),
		'all_items'             => __( 'All Jobs', 'bwm' ),
		'parent_item'           => __( 'Parent Job', 'bwm' ),
		'parent_item_colon'     => __( 'Parent Job', 'bwm' ),
		'edit_item'             => __( 'Edit Job', 'bwm' ),
		'update_item'           => __( 'Update Job', 'bwm' ),
		'add_new_item'          => __( 'Add New Job', 'bwm' ),
		'new_item_name'         => __( 'New Job Name', 'bwm' ),
		'add_or_remove_items'   => __( 'Add or remove Jobs', 'bwm' ),
		'choose_from_most_used' => __( 'Choose from most used Jobs', 'bwm' ),
		'menu_name'             => __( 'Jobs', 'bwm' ),
	);

	$team = get_post_type_object( 'team' );

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'show_in_rest'      => true,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug'       => $team->has_archive,
			'with_front' => false
		),
//		'capabilities'      => array(),
	);

	register_taxonomy( 'team_job', array( 'team' ), $args );
}

add_action( 'init', 'bwm_register_team_job_taxonomy' );
