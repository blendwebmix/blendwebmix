<?php

/*
 * Plugin Name: Freelance management
 * Description: Allow freelance management in WordPress
 * Version: 1.0.0
 * Author: Florian Truchot
 * Author URI: http://www.truchot.co
 * Text Domain: bwm
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


if ( ! function_exists( 'bwm_i18n_setup' ) ) {

	/**
	 * Load plugin textdomain.
	 *
	 * @since 1.0.0
	 */
	function bwm_i18n_setup() {
		load_muplugin_textdomain( 'bwm', 'languages' );
	}

	add_action( 'plugins_loaded', 'bwm_i18n_setup' );
}


/**
 * Registers a new post type
 *
 * @param string Freelances
 * @param array|string See optional args description above.
 *
 * @return object|WP_Error the registered post type object, or an error object
 * @uses $wp_post_types Inserts new post type object into the list
 *
 */
function bwm_register_freelance_post_type() {

	$labels = array(
		'name'               => __( 'Freelances', 'bwm' ),
		'singular_name'      => __( 'Freelance', 'bwm' ),
		'add_new'            => __( 'Add new', 'bwm' ),
		'add_new_item'       => __( 'Add new freelance', 'bwm' ),
		'edit_item'          => __( 'Edit freelance', 'bwm' ),
		'new_item'           => __( 'New freelance', 'bwm' ),
		'view_item'          => __( 'View freelance', 'bwm' ),
		'search_items'       => __( 'Search freelances', 'bwm' ),
		'not_found'          => __( 'No freelances found', 'bwm' ),
		'not_found_in_trash' => __( 'No freelances found in Trash', 'bwm' ),
		'parent_item_colon'  => __( 'Parent freelance:', 'bwm' ),
		'menu_name'          => __( 'Freelances', 'bwm' ),
	);

	$cpt_slug = _x( 'freelances', 'Archive page slug', 'bwm' );

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array( 'freelance_service' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_rest'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-microphone',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'has_archive'         => $cpt_slug,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array(
			'slug'       => $cpt_slug . '/%freelance_service%',
			'with_front' => false
		),
		'capability_type'     => 'post',
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' )
	);

	register_post_type( 'freelance', $args );

}

add_action( 'init', 'bwm_register_freelance_post_type' );


/**
 * Create a taxonomy
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 *
 * @return null|WP_Error WP_Error if errors, otherwise null.
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 */
function bwm_register_freelance_service_taxonomy() {

	$labels = array(
		'name'                  => _x( 'Services', 'Sessions types', 'bwm' ),
		'singular_name'         => _x( 'Service', 'Sessions type', 'bwm' ),
		'search_items'          => __( 'Search Services', 'bwm' ),
		'popular_items'         => __( 'Popular Services', 'bwm' ),
		'all_items'             => __( 'All Services', 'bwm' ),
		'parent_item'           => __( 'Parent Service', 'bwm' ),
		'parent_item_colon'     => __( 'Parent Service', 'bwm' ),
		'edit_item'             => __( 'Edit Service', 'bwm' ),
		'update_item'           => __( 'Update Service', 'bwm' ),
		'add_new_item'          => __( 'Add New Service', 'bwm' ),
		'new_item_name'         => __( 'New Service Name', 'bwm' ),
		'add_or_remove_items'   => __( 'Add or remove Services', 'bwm' ),
		'choose_from_most_used' => __( 'Choose from most used Services', 'bwm' ),
		'menu_name'             => __( 'Services', 'bwm' ),
	);

	$freelance = get_post_type_object( 'freelance' );

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug'       => $freelance->has_archive,
			'with_front' => false
		),
	);

	register_taxonomy( 'freelance_service', array( 'freelance' ), $args );
}

add_action( 'init', 'bwm_register_freelance_service_taxonomy' );


/**
 * Generate correct session URI
 *
 * @param $post_link
 * @param $id
 *
 * @return array Modified taxonomy args
 * @since 1.0
 */

function bwm_freelance_link( $post_link, $id = 0 ) {

	$post = get_post( $id );

	if ( strpos( $post_link, '%freelance_service%' ) === false ) {
		return $post_link;
	}

	// Get the genre
	$terms = get_the_terms( $post->ID, 'freelance_service' );

	if ( is_wp_error( $terms ) || ! $terms ) {
		$freelance_service = 'unknown';
	} else {
		$session_type_obj  = array_pop( $terms );
		$freelance_service = $session_type_obj->slug;
	}

	$post_link = str_replace( '%freelance_service%', $freelance_service, $post_link );

	return $post_link;

}

add_filter( 'post_type_link', 'bwm_freelance_link', 10, 2 );
