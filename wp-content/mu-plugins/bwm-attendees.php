<?php

/*
 * Plugin Name: Attendees management
 * Description: Allow attendees management in WordPress
 * Version: 1.0.0
 * Author: Nico Prat
 * Author URI: http://www.nicooprat.com
 * Text Domain: bwm
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


if ( ! function_exists( 'bwm_i18n_setup' ) ) {

	/**
	 * Load plugin textdomain.
	 *
	 * @since 1.0.0
	 */
	function bwm_i18n_setup() {
		load_muplugin_textdomain( 'bwm', 'languages' );
	}

	add_action( 'plugins_loaded', 'bwm_i18n_setup' );
}


/**
 * Registers a new post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string Attendees
 * @param array|string See optional args description above.
 *
 * @return object|WP_Error the registered post type object, or an error object
 */
function bwm_register_attendee_post_type() {

	$labels = array(
		'name'               => __( 'Attendees', 'bwm' ),
		'singular_name'      => __( 'Attendee', 'bwm' ),
		'add_new'            => __( 'Add new', 'bwm' ),
		'add_new_item'       => __( 'Add new attendee', 'bwm' ),
		'edit_item'          => __( 'Edit attendee', 'bwm' ),
		'new_item'           => __( 'New attendee', 'bwm' ),
		'view_item'          => __( 'View attendee', 'bwm' ),
		'search_items'       => __( 'Search attendees', 'bwm' ),
		'not_found'          => __( 'No attendees found', 'bwm' ),
		'not_found_in_trash' => __( 'No attendees found in Trash', 'bwm' ),
		'parent_item_colon'  => __( 'Parent attendee:', 'bwm' ),
		'menu_name'          => __( 'Attendees', 'bwm' ),
	);

	$cpt_slug = __( 'attendees', 'bwm' );


	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_rest'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-universal-access',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => $cpt_slug,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array(
			'slug'       => $cpt_slug,
			'with_front' => false
		),
		'capability_type'     => 'post',
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' )
	);

	register_post_type( 'attendee', $args );

}

add_action( 'init', 'bwm_register_attendee_post_type' );
