<?php

/*
 * Plugin Name: Session management
 * Description: Allow session management in WordPress
 * Version: 1.3.2
 * Author: Florian Truchot
 * Author URI: http://www.truchot.co
 * Text Domain: bwm
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


if ( ! function_exists( 'bwm_i18n_setup' ) ) {

	/**
	 * Load plugin textdomain.
	 *
	 * @since 1.0.0
	 */
	function bwm_i18n_setup() {
		load_muplugin_textdomain( 'bwm', 'languages' );
	}

	add_action( 'plugins_loaded', 'bwm_i18n_setup' );
}


/**
 * Registers a new post type
 *
 * @param string Sessions
 * @param array|string See optional args description above.
 *
 * @return object|WP_Error the registered post type object, or an error object
 * @uses $wp_post_types Inserts new post type object into the list
 *
 */
function bwm_register_session_post_type() {

	$labels = array(
		'name'               => __( 'Sessions', 'bwm' ),
		'singular_name'      => __( 'Session', 'bwm' ),
		'add_new'            => __( 'Add new', 'bwm' ),
		'add_new_item'       => __( 'Add new session', 'bwm' ),
		'edit_item'          => __( 'Edit session', 'bwm' ),
		'new_item'           => __( 'New session', 'bwm' ),
		'view_item'          => __( 'View session', 'bwm' ),
		'search_items'       => __( 'Search sessions', 'bwm' ),
		'not_found'          => __( 'No sessions found', 'bwm' ),
		'not_found_in_trash' => __( 'No sessions found in Trash', 'bwm' ),
		'parent_item_colon'  => __( 'Parent session:', 'bwm' ),
		'menu_name'          => __( 'Sessions', 'bwm' ),
	);

	$cpt_slug = __( 'program', 'bwm' );

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_rest'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-list-view',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => $cpt_slug,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array(
			'slug'       => $cpt_slug . '/%session_type%',
			'with_front' => false
		),
		'capability_type'     => 'post',
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' )
	);

	register_post_type( 'session', $args );

}

add_action( 'init', 'bwm_register_session_post_type' );


/**
 * Generate correct session URI
 *
 * @param $post_link
 * @param $id
 *
 * @return array Modified taxonomy args
 * @since 1.0
 */

function bwm_session_link( $post_link, $id = 0 ) {

	$post = get_post( $id );

	if ( strpos( $post_link, '%session_type%' ) === false ) {
		return $post_link;
	}

	// Get the genre
	$terms = get_the_terms( $post->ID, 'session_type' );

	if ( is_wp_error( $terms ) || ! $terms ) {
		$session_type = 'unknown';
	} else {
		$session_type_obj = array_pop( $terms );
		$session_type     = $session_type_obj->slug;
	}

	$post_link = str_replace( '%session_type%', $session_type, $post_link );

	return $post_link;

}

add_filter( 'post_type_link', 'bwm_session_link', 10, 2 );


/**
 * Create a taxonomy
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 *
 * @return null|WP_Error WP_Error if errors, otherwise null.
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 */
function bwm_register_session_theme_taxonomy() {

	$labels = array(
		'name'                  => _x( 'Themes', 'Sessions types', 'bwm' ),
		'singular_name'         => _x( 'Theme', 'Sessions type', 'bwm' ),
		'search_items'          => __( 'Search Themes', 'bwm' ),
		'popular_items'         => __( 'Popular Themes', 'bwm' ),
		'all_items'             => __( 'All Themes', 'bwm' ),
		'parent_item'           => __( 'Parent Theme', 'bwm' ),
		'parent_item_colon'     => __( 'Parent Theme', 'bwm' ),
		'edit_item'             => __( 'Edit Theme', 'bwm' ),
		'update_item'           => __( 'Update Theme', 'bwm' ),
		'add_new_item'          => __( 'Add New Theme', 'bwm' ),
		'new_item_name'         => __( 'New Theme Name', 'bwm' ),
		'add_or_remove_items'   => __( 'Add or remove Themes', 'bwm' ),
		'choose_from_most_used' => __( 'Choose from most used Themes', 'bwm' ),
		'menu_name'             => __( 'Theme', 'bwm' ),
	);

	$session = get_post_type_object( 'session' );

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'show_in_rest'      => true,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => $session->has_archive
		),
		'query_var'         => true,
//		'capabilities'      => array(),
	);

	register_taxonomy( 'session_theme', array( 'session', 'speaker' ), $args );
}

add_action( 'init', 'bwm_register_session_theme_taxonomy' );


/**
 * Create a taxonomy
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 *
 * @return null|WP_Error WP_Error if errors, otherwise null.
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 */
function bwm_register_session_room_taxonomy() {

	$labels = array(
		'name'                  => _x( 'Rooms', 'Sessions types', 'bwm' ),
		'singular_name'         => _x( 'Room', 'Sessions type', 'bwm' ),
		'search_items'          => __( 'Search Rooms', 'bwm' ),
		'popular_items'         => __( 'Popular Rooms', 'bwm' ),
		'all_items'             => __( 'All Rooms', 'bwm' ),
		'parent_item'           => __( 'Parent Room', 'bwm' ),
		'parent_item_colon'     => __( 'Parent Room', 'bwm' ),
		'edit_item'             => __( 'Edit Room', 'bwm' ),
		'update_item'           => __( 'Update Room', 'bwm' ),
		'add_new_item'          => __( 'Add New Room', 'bwm' ),
		'new_item_name'         => __( 'New Room Name', 'bwm' ),
		'add_or_remove_items'   => __( 'Add or remove Rooms', 'bwm' ),
		'choose_from_most_used' => __( 'Choose from most used Rooms', 'bwm' ),
		'menu_name'             => __( 'Room', 'bwm' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => __( 'rooms', 'bwm' ),
		),
		'query_var'         => true,
//		'capabilities'      => array(),
	);

	register_taxonomy( 'session_room', array( 'session' ), $args );
}

add_action( 'init', 'bwm_register_session_room_taxonomy' );


/**
 * Create a taxonomy
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 *
 * @return null|WP_Error WP_Error if errors, otherwise null.
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 */
function bwm_register_session_day_taxonomy() {

	$labels = array(
		'name'                  => _x( 'Days', 'Sessions days', 'bwm' ),
		'singular_name'         => _x( 'Day', 'Sessions day', 'bwm' ),
		'search_items'          => __( 'Search Days', 'bwm' ),
		'popular_items'         => __( 'Popular Days', 'bwm' ),
		'all_items'             => __( 'All Days', 'bwm' ),
		'parent_item'           => __( 'Parent Day', 'bwm' ),
		'parent_item_colon'     => __( 'Parent Day', 'bwm' ),
		'edit_item'             => __( 'Edit Day', 'bwm' ),
		'update_item'           => __( 'Update Day', 'bwm' ),
		'add_new_item'          => __( 'Add New Day', 'bwm' ),
		'new_item_name'         => __( 'New Day Name', 'bwm' ),
		'add_or_remove_items'   => __( 'Add or remove Days', 'bwm' ),
		'choose_from_most_used' => __( 'Choose from most used Days', 'bwm' ),
		'menu_name'             => __( 'Day', 'bwm' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => __( 'days', 'bwm' ),
		),
		'query_var'         => true,
//		'capabilities'      => array(),
	);

	register_taxonomy( 'session_day', array( 'session' ), $args );
}

add_action( 'init', 'bwm_register_session_day_taxonomy' );


/**
 * Create a taxonomy
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 *
 * @return null|WP_Error WP_Error if errors, otherwise null.
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 */
function bwm_register_session_type_taxonomy() {

	$labels = array(
		'name'                  => _x( 'Types', 'Sessions types', 'bwm' ),
		'singular_name'         => _x( 'Type', 'Sessions type', 'bwm' ),
		'search_items'          => __( 'Search Types', 'bwm' ),
		'popular_items'         => __( 'Popular Types', 'bwm' ),
		'all_items'             => __( 'All Types', 'bwm' ),
		'parent_item'           => __( 'Parent Type', 'bwm' ),
		'parent_item_colon'     => __( 'Parent Type', 'bwm' ),
		'edit_item'             => __( 'Edit Type', 'bwm' ),
		'update_item'           => __( 'Update Type', 'bwm' ),
		'add_new_item'          => __( 'Add New Type', 'bwm' ),
		'new_item_name'         => __( 'New Type Name', 'bwm' ),
		'add_or_remove_items'   => __( 'Add or remove Types', 'bwm' ),
		'choose_from_most_used' => __( 'Choose from most used Days', 'bwm' ),
		'menu_name'             => __( 'Type', 'bwm' ),
	);

	$session = get_post_type_object( 'session' );

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array(
			'with_front' => false,
			'slug'       => $session->has_archive
		),
		'query_var'         => true,
	);

	register_taxonomy( 'session_type', array( 'session' ), $args );
}

add_action( 'init', 'bwm_register_session_type_taxonomy' );


/**
 * When session saved sync session theme to speaker
 *
 * @param $post_id
 *
 * @return mixed
 */
function bwm_sync_speaker_session_theme( $post_id ) {

	$theme    = get_field( 'session_theme', $post_id );
	$speakers = get_field( 'session_speaker', $post_id );

	foreach ( $speakers as $speaker ) {
		wp_set_object_terms( $speaker->ID, array( $theme->term_id ), 'session_theme' );
	}

}

// acf/update_value - filter for every field
add_filter( 'acf/save_post', 'bwm_sync_speaker_session_theme' );



/**
 * When session is remove, delete session on swapcard
 *
 * @param $post_id
 *
 * @return mixed
 */
function bwm_delete_session_swapcard($post_id) { 
	
	$swapcard_id = get_field( 'swapcard_id', $post_id );

	if(is_null($swapcard_id)) {
		return;
	}
	
	if( !defined('SWAPCARD_CONNECTOR_API_EVENT_ID')){
		return;
	}

	$evendId = SWAPCARD_CONNECTOR_API_EVENT_ID;

	// headers & url of the request
	$url = SWAPCARD_CONNECTOR_API_URL;
	$headers = array(
		'Content-Type' => 'application/json',
		'Authorization' => SWAPCARD_CONNECTOR_API_ACCESS_TOKEN
	);

	$variables = array(
		'planningsIds' => $swapcard_id,
		'eventId' 	=> $evendId
	);

	// create graphql mutation request
	$body = wp_json_encode([
        'query' => '
			mutation deleteEventPlannings($eventId: String!, $planningsIds: [String!]!) {
				deleteEventPlannings(eventId: $eventId, planningsIds: $planningsIds) { id }
			}
		',
		'variables' => $variables
	]);

	// create the http request
	$response = wp_remote_post($url, array(
        'method' => 'POST',
        'headers' => $headers,
		'body' => $body
	));

}

add_action( 'wp_trash_post', 'bwm_delete_session_swapcard' );


/**
 * When session saved, sync session on swapcard
 *
 * @param $post_id
 *
 * @return mixed
 */
function bwm_sync_speaker_session_swapcard( $post_id ) {

	if( !defined('SWAPCARD_CONNECTOR_API_EVENT_ID')){
		return;
	}

	$theme    			= get_field( 'session_theme', $post_id );
	$session_speaker	= get_field( 'session_speaker', $post_id );
	$session_start		= get_field( 'session_start', $post_id );
	$session_end		= get_field( 'session_end', $post_id );
	$type 				= get_field( 'type', $post_id );
	$session_room		= get_field( 'session_room', $post_id);
	$swapcard_id		= get_field( 'swapcard_id', $post_id );
	$post_title 		= get_the_title( $post_id );
	$post_content 		= get_post_field('post_content', $post_id);

	$speakers = array();

	// get the swapcard_speaker_id of each speaker
	foreach ( $session_speaker as $speaker ) {
		$swapcard_speaker_id = get_field( 'swapcard_speaker_id', $speaker->ID);
		if(!empty($swapcard_speaker_id)) {
			array_push($speakers, $swapcard_speaker_id);
		}
	}

	//Mapper between WP ACF object and swapcard object
	$planning->hash="1";
	$planning->title=$post_title;
	$planning->description=$post_content;
	$planning->beginsAt=$session_start;
	$planning->endsAt=$session_end;
	$planning->type=$type->name;
	$planning->place=$session_room->name;
	$planning->categories=array($theme->name);
	$planning->speakersIds=$speakers;

	// If swapcard_id is not empty the event will be update else it will be create
	if (!empty($swapcard_id)) {
		$planning->id=$swapcard_id;
	}
	

	$evendId = SWAPCARD_CONNECTOR_API_EVENT_ID;

	// headers & url of the request
	$url = SWAPCARD_CONNECTOR_API_URL;
	$headers = array(
		'Content-Type' => 'application/json',
		'Authorization' => SWAPCARD_CONNECTOR_API_ACCESS_TOKEN
	);

	$variables = array(
		'plannings' => $planning,
		'eventId' 	=> $evendId
	);

	// create graphql mutation request
	$body = wp_json_encode([
        'query' => '
			mutation upsertPlannings($eventId: String!, $plannings: [UpsertEventPlanningInput!]!) {
				upsertEventPlannings(eventId: $eventId, plannings: $plannings) { id }
			}
		',
		'variables' => $variables
	]);

	// create the http request
	$response = wp_remote_post($url, array(
        'method' => 'POST',
        'headers' => $headers,
		'body' => $body
	));

	//If new session is created, add swapcard_id to wordpress
	if (empty($swapcard_id)) {
		$decoded_response = json_decode( $response['body'], true );
		$id_planning = $decoded_response['data']['upsertEventPlannings'][0]['id'];
		update_field('swapcard_id',$id_planning ,$post_id);
	}
}


// acf/update_value - filter for every field
add_action( 'acf/save_post', 'bwm_sync_speaker_session_swapcard', 20);



/**
 * [Disabled] Snycro between wordpress speakers & swapcard speackers 
 */

/**
if ( is_admin() && isset($_GET['update_sessions'])) {
    add_action('init', 'update_session_function', 10);
}

// Update sessions with swapcard planning id
function update_session_function() {

	if( !defined('SWAPCARD_CONNECTOR_API_EVENT_ID')){
		return;
	}

	$evendId = SWAPCARD_CONNECTOR_API_EVENT_ID;

	// headers & url of the request
	$url = SWAPCARD_CONNECTOR_API_URL;
	$headers = array(
		'Content-Type' => 'application/json',
		'Authorization' => SWAPCARD_CONNECTOR_API_ACCESS_TOKEN
	);

	$variables = array(
		'eventId' 	=> $evendId
	);

	// create graphql mutation request
	$body = wp_json_encode([
        'query' => '
			query MyQuery($eventId: String!) {
                plannings(eventId: $eventId, page: 1, pageSize: 1000) {
                    id,
                    title
                }
			}
		',
		'variables' => $variables
	]);

	// create the http request
	$response = wp_remote_post($url, array(
        'method' => 'POST',
        'headers' => $headers,
		'body' => $body
	));

	$decoded_response = json_decode( $response['body'], true );

    // get all session posts
	$posts = get_posts([
		'post_type' => 'session',
		'post_status' => 'publish',
		'numberposts' => -1
	  ]);

	$sessions_not_updated = array();

	var_dump("Session updated with swapcard ID : <ul>");
    foreach ($posts as $post) {
		foreach ( $decoded_response['data']['plannings'] as $planning) {
            // if is the same sessions between wordpress and swapcard
           if (strtolower($planning['title']) == strtolower($post->post_title)) {
               // if the session has already swapcar_id
               if (empty(get_field( 'swapcard_id', $post->ID))) {
				   var_dump("<li>{$planning['title']}</li>");
                   update_field('swapcard_id',$planning['id'] ,$post->ID);
               } 
		   } else {
			   if(!in_array($post->post_title, $sessions_not_updated) && empty(get_field( 'swapcard_id', $post->ID))){
				   $sessions_not_updated[]=$post->post_title;
				}
			}
        }
	}
	var_dump("</ul>");

	var_dump("Session not updated with swapcar ID : <ul>");
	foreach( $sessions_not_updated as $session) {
		var_dump("<li>{$session}</li>");
	}
	var_dump("</ul>");
    die;
}
*/