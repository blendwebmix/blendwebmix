<?php

/*
 * Plugin Name: Speaker management
 * Description: Allow speaker management in WordPress
 * Version: 1.0.0
 * Author: Florian Truchot
 * Author URI: http://www.truchot.co
 * Text Domain: bwm
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


if ( ! function_exists( 'bwm_i18n_setup' ) ) {

	/**
	 * Load plugin textdomain.
	 *
	 * @since 1.0.0
	 */
	function bwm_i18n_setup() {
		load_muplugin_textdomain( 'bwm', 'languages' );
	}

	add_action( 'plugins_loaded', 'bwm_i18n_setup' );
}


/**
 * Registers a new post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string Speakers
 * @param array|string See optional args description above.
 *
 * @return object|WP_Error the registered post type object, or an error object
 */
function bwm_register_speaker_post_type() {

	$labels = array(
		'name'               => __( 'Speakers', 'bwm' ),
		'singular_name'      => __( 'Speaker', 'bwm' ),
		'add_new'            => __( 'Add new', 'bwm' ),
		'add_new_item'       => __( 'Add new speaker', 'bwm' ),
		'edit_item'          => __( 'Edit speaker', 'bwm' ),
		'new_item'           => __( 'New speaker', 'bwm' ),
		'view_item'          => __( 'View speaker', 'bwm' ),
		'search_items'       => __( 'Search speakers', 'bwm' ),
		'not_found'          => __( 'No speakers found', 'bwm' ),
		'not_found_in_trash' => __( 'No speakers found in Trash', 'bwm' ),
		'parent_item_colon'  => __( 'Parent speaker:', 'bwm' ),
		'menu_name'          => __( 'Speakers', 'bwm' ),
	);


	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_rest'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-microphone',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => _x( 'speakers', 'Archive page slug', 'bwm' ),
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array(
			'slug'       => _x( 'speakers', 'Single page slug', 'bwm' ),
			'with_front' => false
		),
		'capability_type'     => 'post',
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' )
	);

	register_post_type( 'speaker', $args );

}

add_action( 'init', 'bwm_register_speaker_post_type' );

/**
 * When speaker is remove delete speaker on swapcard
 *
 * @param $post_id
 *
 * @return mixed
 */
function bwm_delete_speaker_swapcard($post_id) { 
	
	$swapcard_speaker_id = get_field( 'swapcard_speaker_id', $post_id );

	if(is_null($swapcard_speaker_id)) {
		return;
	}
	
	if( !defined('SWAPCARD_CONNECTOR_API_EVENT_ID')){
		return;
	}

	$evendId = SWAPCARD_CONNECTOR_API_EVENT_ID;

	// headers & url of the request
	$url = SWAPCARD_CONNECTOR_API_URL;
	$headers = array(
		'Content-Type' => 'application/json',
		'Authorization' => SWAPCARD_CONNECTOR_API_ACCESS_TOKEN
	);

	$variables = array(
		'eventPeopleIds' => array($swapcard_speaker_id),
		'eventId' 	=> $evendId
	);

	// create graphql mutation request
	$body = wp_json_encode([
		'query' => '
			mutation deleteEventPeople($eventId: ID!, $eventPeopleIds: [ID!]!) {
				deleteEventPeople(eventId: $eventId, eventPeopleIds: $eventPeopleIds) {
				eventPeopleDeleted
				}
			}
		',
		'variables' => $variables
	]);

	// create the http request
	$response = wp_remote_post($url, array(
        'method' => 'POST',
        'headers' => $headers,
		'body' => $body
	));

}

add_action( 'wp_trash_post', 'bwm_delete_speaker_swapcard' );




/**
 * When speaker saved, sync speaker on swapcard
 *
 * @param $post_id
 *
 * @return mixed
 */
function bwm_sync_speaker_swapcard( $post_id ) {

	if( !defined('SWAPCARD_CONNECTOR_API_EVENT_ID')){
		return;
	}

	// Get all values form the post
	$speaker_job    		= get_field( 'speaker_job', $post_id );
	$speaker_company		= get_field( 'speaker_company', $post_id );
	$speaker_firstName		= get_field( 'speaker_firstName', $post_id );
	$speaker_lastName		= get_field( 'speaker_lastName', $post_id );
	$speaker_img 			= get_field( 'speaker_img', $post_id );
	$speaker_biography		= get_field( 'speaker_bio', $post_id );
	$swapcard_speaker_id	= get_field( 'swapcard_speaker_id', $post_id );
	 
	// If swapcard_speaker_id is not empty the speaker will be update else it will be create
	if (!empty($swapcard_speaker_id)) {
		//Mapper between WP ACF object and swapcard object
		$data->id=$swapcard_speaker_id;
		$data->update->isUser=false;
		$data->update->firstName=$speaker_firstName;
		$data->update->lastName=$speaker_lastName;
		$data->update->jobTitle=$speaker_job;
		$data->update->organization=$speaker_company;
		$data->update->biography=$speaker_biography;
		$data->update->photoUrl=$speaker_img['url'];
	} else {
		//Mapper between WP ACF object and swapcard object
		$data->create->isUser=false;
		$data->create->firstName=$speaker_firstName;
		$data->create->lastName=$speaker_lastName;
		$data->create->jobTitle=$speaker_job;
		$data->create->organization=$speaker_company;
		$data->create->biography=$speaker_biography;
		$data->create->photoUrl=$speaker_img['url'];
		
		// Add speaker to speaker group
		$data->actions->updateGroups->action="ADD";
		$data->actions->updateGroups->groupIds=array(SWAPCARD_CONNECTOR_API_SPEAKER_GROUP_ID);
	}

	$evendId = SWAPCARD_CONNECTOR_API_EVENT_ID;

	// headers & url of the request
	$url = SWAPCARD_CONNECTOR_API_URL;
	$headers = array(
		'Content-Type' 	=> 'application/json',
		'Authorization' => SWAPCARD_CONNECTOR_API_ACCESS_TOKEN
	);

	$variables = array(
		'data' 		=> array($data),
		'eventId' 	=> $evendId
	);

	// create graphql mutation request
	$body = wp_json_encode([
		'query' => '
			mutation importEventPeople($eventId: ID!, $data: [ImportEventPersonInput!]!) {
				importEventPeople(eventId: $eventId, data: $data) {
					  eventPeopleCreated
					  eventPeopleUpdated
				
				}
			}
		',
		'variables' => $variables
	]);

	// create the http request
	$response = wp_remote_post($url, array(
        'method' => 'POST',
        'headers' => $headers,
		'body' => $body
	));

	//If new speaker is created, add swapcard_id to wordpress
	if (empty($swapcard_speaker_id)) {
		$decoded_response = json_decode( $response['body'], true );
		$swapcard_speaker_id = $decoded_response['data']['importEventPeople']['eventPeopleCreated'][0];
		update_field('swapcard_speaker_id',$swapcard_speaker_id ,$post_id);
	}
}

// acf/update_value - filter for every field
add_action( 'acf/save_post', 'bwm_sync_speaker_swapcard', 20);


/**
 * [Disabled] Snycro between wordpress speakers & swapcard speackers 
 */

 /**
if ( is_admin() && isset($_GET['update_speakers'])) {
    add_action('init', 'update_speakers_function', 10);
}

// Update speakers with swapcard speakers id
function update_speakers_function() {

    if(!defined('SWAPCARD_CONNECTOR_API_EVENT_ID')){
		return;
	}

	$evendId = SWAPCARD_CONNECTOR_API_EVENT_ID;

	// headers & url of the request
	$url = SWAPCARD_CONNECTOR_API_URL;
	$headers = array(
		'Content-Type' => 'application/json',
		'Authorization' => SWAPCARD_CONNECTOR_API_ACCESS_TOKEN
	);

	$variables = array(
		'eventId' 	=> $evendId
	);

	// create graphql mutation request
	$body = wp_json_encode([
        'query' => '
			query MyQuery($eventId: ID!) {
				eventPeople(eventId: $eventId, page: 1, pageSize: 1000) {
					nodes {
						id,
						lastName,
						firstName
					}
				}
			}
		',
		'variables' => $variables
	]);

	// create the http request
	$response = wp_remote_post($url, array(
        'method' => 'POST',
        'headers' => $headers,
		'body' => $body
	));

	$decoded_response = json_decode( $response['body'], true );

     // get all speakers posts
	$speakers = get_posts([
		'post_type' => 'speaker',
		'post_status' => 'publish',
		'numberposts' => -1
      ]);
      

      foreach ( $decoded_response['data']['eventPeople']['nodes'] as $people) {;
        $fullName = "{$people['firstName']} {$people['lastName']}";
        foreach ($speakers as $speaker) {
            // if are the same speakers between wordpress and swapcard
           if (strtolower($fullName) == strtolower($speaker->post_title)) {
               // if the speaker has already speaker swapcar id
               if (empty(get_field( 'swapcard_speaker_id', $speaker->ID))) {
				   var_dump($fullName);
                   update_field('swapcard_speaker_id',$people['id'] ,$speaker->ID);
               } 
           }
        }
    }

    var_dump("Speakers updated with speakers swapcard ID");
    die;
}
 */