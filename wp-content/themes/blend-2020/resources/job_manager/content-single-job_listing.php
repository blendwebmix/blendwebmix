<?php
/**
 * Single job listing.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-single-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @since       1.0.0
 * @version     1.28.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

global $post;

?>

<div class="single_job_listing">
    <?php
    if (get_the_company_name()) {

        the_company_name(
            '<h1>' . get_the_title() . '<span>',
            '</span></h1>');
    }
    ?>
    <div class="single_job_body">
        <?php if (get_option('job_manager_hide_expired_content', 1) && 'expired' === $post->post_status) : ?>
            <div class="job-manager-info"><?php _e('This listing has expired.', 'wp-job-manager'); ?></div>
        <?php else : ?>
            <div class="single_job_top">
                <div class="job_logo">
                    <div class="job_logo_container">
                        <?php the_company_logo(); ?>
                    </div>
                </div>
                <div>
                    <div>
                        <?php if (get_option('job_manager_enable_types')) { ?>
                            <?php $types = wpjm_get_the_job_types(); ?>
                            <?php if (!empty($types)) : foreach ($types as $type) : ?>
                                <span
                                    class="job-type <?php echo esc_attr(sanitize_title($type->slug)); ?>"><?php echo esc_html($type->name); ?></span>
                            <?php endforeach; endif; ?>
                        <?php } ?>

                        <span class="location"><?php the_job_location(); ?></span>
                    </div>
                    <?php the_company_tagline('<p class="tagline"><strong>', '</strong></p>'); ?>
                    <?php if ($website = get_the_company_website()) : ?>
                        <a class="website" href="<?php echo esc_url($website); ?>" target="_blank"
                           rel="nofollow"><?php esc_html_e('Website', 'wp-job-manager'); ?></a>
                    <?php endif; ?>
                    <?php the_company_twitter(); ?>
                </div>
                <?php
                /* <li class="date-posted"><?php the_job_publish_date(); ?></li>*/
                ?>

                <?php if (is_position_filled()) : ?>
                    <li class="position-filled"><?php _e('This position has been filled', 'wp-job-manager'); ?></li>
                <?php elseif (!candidates_can_apply() && 'preview' !== $post->post_status) : ?>
                    <li class="listing-expired"><?php _e('Applications have closed', 'wp-job-manager'); ?></li>
                <?php endif; ?>
            </div>
            <div>
                <h4>Le job</h4>
                <?php the_company_video(); ?>

                <div class="job_description">
                    <?php wpjm_the_job_description(); ?>
                </div>

                <?php if (candidates_can_apply()) : ?>
                    <div class="button-apply">
                        <?php get_job_manager_template('job-application.php'); ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
