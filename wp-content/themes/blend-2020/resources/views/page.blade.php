@extends('layouts.base')

@section('content')
  @while( have_posts() ) @php( the_post() )
    <main class="entry-home">
      @php( the_content() )
    </main>
  @endwhile
@endsection
