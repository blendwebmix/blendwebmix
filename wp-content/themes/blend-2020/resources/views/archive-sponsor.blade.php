@extends('layouts.base')

@section('content')

  <main class="entry-archive">
      <?php
      // Get current archive
      $queried_object = get_queried_object();
      $args = array(
        'name'        => $queried_object->has_archive,
        'post_type'   => 'page',
        'post_status' => 'publish',
        'numberposts' => 1,
      );
      $page = get_posts( $args );

      if (! empty( $page )) :
        $page = current( $page );
        $post_id = $page->ID;
        if ( function_exists( 'pll_get_post' ) ) {
          $post_id = pll_get_post( $page->ID );
        }
        $post = get_post( $post_id );

        if (! empty( $post )): ?>

          <section class="page-content">
            <?php setup_postdata( $post ); ?>
            <h1>{!! get_the_title($post_id) !!}</h1>
            <?php the_content() ?>
            <?php wp_reset_postdata(); ?>
          </section>

        <?php endif; ?>

      <?php else: ?>
        <h1><?php echo $queried_object->labels->all_items; ?></h1>
      <?php endif; ?>

  </main>

@endsection
