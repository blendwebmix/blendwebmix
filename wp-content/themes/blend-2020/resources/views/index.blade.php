@extends('layouts.base')

@section('content')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'bwm') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts()) @php(the_post())
  @include ('partials.content-'.(get_post_type() !== 'post' ? get_post_type() : get_post_format()))
  @endwhile

  <h2 class="navigation-title">{{pll__('navigation.title')}}</h2>
  {!! numeric_posts_nav() !!}
@endsection
