@extends('layouts.base')

@section('content')

  <main class="content entry-archive">
    
    @while (have_posts()) @php(the_post())
      @include ('partials.content-post')
    @endwhile

    <h2 class="navigation-title">{{pll__('navigation.title')}}</h2>
    {!! numeric_posts_nav() !!}
  </main>
@endsection
