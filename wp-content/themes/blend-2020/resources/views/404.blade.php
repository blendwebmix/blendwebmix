@extends('layouts.base')

@section('content')
<article class="error404">

    <h1 class="error404__title">
			{{__('Page not found', 'bmw')}}
		</h1>
		<a href="{{get_bloginfo('url')}}" class="error404__link">{{__('Back to home', 'blend')}}</a>

</article>
@endsection
