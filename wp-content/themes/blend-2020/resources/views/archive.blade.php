@extends('layouts.base')

@section('content')

  <main class="entry-archive">

    <?php

    // Get current archive
    $queried_object = get_queried_object();
    $args = array(
      'name'        => $queried_object->has_archive,
      'post_type'   => 'page',
      'post_status' => 'publish',
      'numberposts' => 1,
    );
    $page = get_posts( $args );

    if (! empty( $page )) :
      $page = current( $page );
      $post_id = $page->ID;
      if ( function_exists( 'pll_get_post' ) ) {
        $post_id = pll_get_post( $page->ID );
      }
      $post = get_post( $post_id );

      if (! empty( $post )): ?>

      <section class="page-content">
        <?php setup_postdata( $post ); ?>
        <h1><?php echo get_the_title($post_id) ?></h1>
        <?php the_content() ?>
        <?php wp_reset_postdata(); ?>
      </section>

      <?php endif; ?>

    <?php endif; ?>

    <?php
    if ( is_post_type_archive( 'speaker' ) ) :
      echo '<h1 class="entry-team__title">' . pll__( 'Les Conférenciers' ) . '</h1>';
    endif;
    ?>

    <div class="archive-content">

      @while (have_posts()) @php(the_post())
      @include ('partials.content-'.( get_post_type() !== 'post' ? get_post_type() : get_post_format() ))
      @endwhile

      <h2 class="navigation-title">{{pll__('navigation.title')}}</h2>
      {!! numeric_posts_nav() !!}

    </div>

  </main>

@endsection
