@php
  $title = get_field('bwm_block_homepage_title_text');
  $subtitle = get_field('bwm_block_homepage_title_subtitle');
  $image = get_field('bwm_block_homepage_title_img');
  $date = get_field('bwm_block_homepage_title_date');
  $back = get_field('bwm_block_homepage_title_promo1_content');
  $back_img = get_field('bwm_block_homepage_title_promo1_bg');
  $front = get_field('bwm_block_homepage_title_promo2_content');
  $front_img = get_field('bwm_block_homepage_title_promo2_bg');
@endphp

<section class="homepage-title">
  <div class="homepage-title__main">
    @if($image)
      @php( $img = wp_get_attachment_image( $image['ID'], 'full', false, ['class' => 'homepage-title__img']))
      {!! $img !!}
    @endif
    <span class="homepage-title__date">{{ $date }}</span>
    <h1 class="homepage-title__title">
      {{ $title  }}
      @if( $subtitle)
        <small class="homepage-title__subtitle">{{ $subtitle }}</small>
      @endif
    </h1>
  </div>

  @if($back)
    <div class="homepage-title__back">
      {!! $back !!}
    </div>
  @endif()

  @if($front)
    <div class="homepage-title__front">
      {!! $front !!}
    </div>
  @endif()

</section>

