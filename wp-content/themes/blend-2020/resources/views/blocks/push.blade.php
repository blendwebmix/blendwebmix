<?php

$title = get_field( 'bwm_block_push_title' );
$detail = get_field( 'bwm_block_push_message' );
$image = get_field( 'bwm_block_push_image' );
$cta_text = get_field( 'bwm_block_push_cta_text' );
$cta_link = get_field( 'bwm_block_push_cta_link' );
$cta_align = get_field( 'bwm_block_push_cta_align' );

?>


<section class="page-push page-push--{{ $cta_align }} @if($image) has-post-thumbnail @endif">
    @if( $title )
        <p class="page-push__message">{{ $title }}</p>
    @endif
    @if( $detail)
        <p class="page-push__detail">{!! do_shortcode($detail) !!}</p>
    @endif
    @if($cta_text && $cta_link)
        <a class="page-push__link" href="{{ $cta_link }}">{{ $cta_text }}</a>
    @endif()
    @if($image)
        @php( $img = wp_get_attachment_image( $image['ID'], 'full', false, ['class' => 'page-push__img']))
        {!! $img !!}
    @endif
</section>
