<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')

<body @php body_class() @endphp>
@if(get_field('google_tag_manager', 'options'))
  <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id={{ get_field('google_tag_manager', 'options') }}"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>
@endif

<div class="wrapper" role="document">

  @php do_action('get_header') @endphp
  @include('partials.header')

  {{--<main class="main">--}}
  @yield('content')
  {{--</main>--}}

  @if (App\display_sidebar())
    <aside class="sidebar">
      @include('partials.sidebar')
    </aside>
  @endif

  @php do_action('get_footer') @endphp
  @include('partials.footer')
  @includeIf('partials.sprite')
  @php wp_footer() @endphp

</div>

<script type="text/javascript">
  _linkedin_partner_id = "463580";
  window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
  window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script>

<script type="text/javascript">
  (function () {
    var s = document.getElementsByTagName("script")[0];
    var b = document.createElement("script");
    b.type = "text/javascript";
    b.async = true;
    b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
    s.parentNode.insertBefore(b, s);
  })();
</script>

<noscript>
  <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=463580&fmt=gif"/>
</noscript>

<script src="<?php bloginfo('template_directory'); ?>/assets/scripts/plugins/jquery.magnific-popup.min.js"></script>
<script type="text/javascript">
  var $ = jQuery.noConflict();
  $(document).ready(function () {
    $('.popup-youtube a').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,

      fixedContentPos: false,
    });
  });
</script>

<!-- This site is converting visitors into subscribers and customers with OptinMonster - https://optinmonster.com -->
<script type="text/javascript" src="https://a.opmnstr.com/app/js/api.min.js" data-account="22217" data-user="32394" async></script>
<!-- / https://optinmonster.com -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WW5T74Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</body>
</html>
