@extends('layouts.base')

@section('content')

  <main class="archiveAttendee content">
    @php
      // Get page with same slug that current taxonomy
      $page = get_posts([
        'name'        => get_queried_object()->has_archive,
        'post_type'   => 'page',
        'post_status' => 'publish',
        'numberposts' => 1,
      ]);

      // Get translation of this page
      if( !empty( $page ) ) :
        $post_id = pll_get_post($page[0]->ID);
        $page = get_post($post_id);
      endif;
    @endphp

    <section class="page-content">

    <?php
      if ( is_post_type_archive( 'attendee' ) ) :
      echo '<h1 class="archiveAttendee-title">' . pll__( 'Les participants' ) . '</h1>';
      endif;

      if (! empty( $page )) {
      setup_postdata( $page );
      the_content();
      wp_reset_postdata();
    }
    ?>

    </section>

  @if(!empty($page) && $page->post_content)
      <article class="archiveAttendee-content">
        {!!wpautop($page->post_content)!!}
      </article>
    @endif

    <ul class="archiveAttendee-list">
      @while (have_posts()) @php(the_post())
        <li class="archiveAttendee-item">
          @if(get_post_thumbnail_id(get_the_ID()))
            @php($crop = resize(get_post_thumbnail_id(get_the_ID()), [300,300], false))
            <figure class="archiveAttendee-item-img">
              <img
                src="{{$crop['src']}}"
                alt="{{get_the_title()}}"
                width="{{$crop['width']}}"
                height="{{$crop['height']}}">
            </figure>
          @endif
          <a href="{{get_field('website')}}" class="archiveAttendee-item-title">
            {!!the_title()!!}
          </a>
          <div class="archiveAttendee-item-desc">
            {!!strip_tags(get_the_content())!!}
          </div>
        </li>
      @endwhile
    </ul>
  </main>
@endsection
