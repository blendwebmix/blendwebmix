<section class="page-push page-push--{{ $alignment }} @if($image) has-post-thumbnail @endif">
  @if( $message)
  <p class="page-push__message">{{ $message }}</p>
  @endif
  @if( $detail)
    <p class="page-push__detail">{!! do_shortcode($detail) !!}</p>
  @endif
  @if($cta_text && $cta_link)
    <a class="page-push__link" href="{{ $cta_link }}">{{ $cta_text }}</a>
  @endif()
  @if($image)
    @php( $img = wp_get_attachment_image( $image['ID'], 'full', false, ['class' => 'page-push__img']))
    {!! $img !!}
  @endif
</section>
