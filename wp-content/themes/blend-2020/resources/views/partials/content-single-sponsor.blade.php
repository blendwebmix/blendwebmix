<article class="container entry-sponsor">
  @if( has_post_thumbnail() )
    <figure class="entry-sponsor__logo">
      @php($crop = resize(get_post_thumbnail_id(get_the_ID()), [1024,null], true))
      <img
        src="{{$crop['src']}}"
        alt="{!! get_the_title() !!}"
        width="{{$crop['width']}}"
        height="{{$crop['height']}}">
    </figure>
  @endif
  <h1 class="entry-sponsor__title">{!! get_the_title() !!}</h1>
  <div class="entry-sponsor__content">
    @php( the_content() )
  </div>
</article>
