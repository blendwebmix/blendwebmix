<article @php(post_class( ["more-entry"], $post->ID))>
  @if( has_post_thumbnail($post->ID) )
    @php($crop = resize(get_post_thumbnail_id($post->ID), [300,300], true))
    <img
      src="{{$crop['src']}}"
      alt="{{$post->post_title}}"
      width="{{$crop['width']}}"
      height="{{$crop['height']}}">
  @endif
  <a href="{{ get_the_permalink( $post->ID ) }}" class="more-entry__title">{{ get_the_title( $post->ID ) }}</a>
    @php( $terms = get_the_terms( $post->ID, 'team_job' ) )

    @if( $terms && ! is_wp_error( $terms ) )

      <?php
      $jobs_term = array();

      foreach ( $terms as $term ) {
        $jobs_term[] = $term->name;
      }

      $jobs = join( ", ", $jobs_term );
      ?>

      <span class="more-entry__job">
        {{ $jobs }}
      </span>
    @endif
</article>
