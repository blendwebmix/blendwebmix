@if( have_rows('bwm_sections', $postID) )
  @while( have_rows('bwm_sections', $postID) )
    @php the_row() @endphp

    @if( get_row_layout() == "bwm_section_homepage_title")
      @php
        $homepage_title = [
          'title' => get_sub_field('bwm_section_homepage_title_text', $postID),
          'subtitle' => get_sub_field('bwm_section_homepage_title_subtitle', $postID),
          'image' => get_sub_field('bwm_section_homepage_title_img', $postID),
          'date' => get_sub_field('bwm_section_homepage_title_date', $postID),
          'back' => get_sub_field('bwm_section_homepage_title_promo1_content', $postID),
          'back_img' => get_sub_field('bwm_section_homepage_title_promo1_bg', $postID),
          'front' => get_sub_field('bwm_section_homepage_title_promo2_content', $postID),
          'front_img' => get_sub_field('bwm_section_homepage_title_promo2_bg', $postID),
        ];
      @endphp
      @include('partials/homepage_title', $homepage_title)
    @endif

    @if( get_row_layout() == "bwm_section_page_push")
      @php
        $page_push = [
          'message' => get_sub_field('bwm_section_page_push_message', $postID),
          'detail' => get_sub_field('bwm_section_page_push_detail', $postID),
          'image' => get_sub_field('bwm_section_page_push_img', $postID),
          'cta_text' => get_sub_field('bwm_section_page_push_cta_text', $postID),
          'cta_link' => get_sub_field('bwm_section_page_push_cta_link', $postID),
          'alignment' => get_sub_field('bwm_section_page_push_alignment', $postID),
        ];
      @endphp
      @include('partials/page_push', $page_push)
    @endif

    @if( get_row_layout() == "bwm_section_page_blog_extract")
      @php
        $blog_extract = [
          'title' => get_sub_field('bwm_section_page_blog_extract_title', $postID),
          'posts' => get_sub_field('bwm_section_page_blog_extract_posts', $postID),
        ];
      @endphp
      @include('partials/blog_extract', $blog_extract)
    @endif

    @if( get_row_layout() == "bwm_section_page_social")
      @php
        $page_social = [
            'message' => get_sub_field('bwm_section_page_social_title', $postID),
        ];
      @endphp
      @include('partials/page_social', $page_social)
    @endif

    @if( get_row_layout() == "bwm_section_page_newsletter")
      @php
        $page_newsletter = [
          'title' => get_sub_field('bwm_section_page_newsletter_title', $postID),
          'subtitle' => get_sub_field('bwm_section_page_newsletter_subtitle', $postID),
          'message' => get_sub_field('bwm_section_page_newsletter_shortcode', $postID),
        ];
      @endphp
      @include('partials/page_newsletter', $page_newsletter)
    @endif

    @if( get_row_layout() == "bwm_section_page_title")
      @php
        $page_image_simple = [
            'title' => get_sub_field('bwm_section_page_title_title', $postID),
            'subtitle' => get_sub_field('bwm_section_page_title_intro', $postID),
            'image' => get_sub_field('bwm_section_page_title_img', $postID),
            'position' => get_sub_field('bwm_section_page_title_alignment', $postID),
        ];
      @endphp
      @include('partials/section_page_title', $page_image_simple)
    @endif

    @if( get_row_layout() == "bwm_section_page_free_content")
      <div class="row">
        @if(get_sub_field('bwm_section_page_content_column1', $postID))
          <div class="column">
            @if(!empty($wpautop) && $wpautop)
              {!!preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '<div class="video-container">\1</div>', wpautop(get_sub_field('bwm_section_page_content_column1', $postID)))!!}
            @else()
              {!!get_sub_field('bwm_section_page_content_column1', $postID)!!}
            @endif
          </div>
        @endif
        @if(get_sub_field('bwm_section_page_content_column2', $postID))
          <div class="column">
            @if(!empty($wpautop) && $wpautop)
              {!!preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '<div class="video-container">\1</div>', wpautop(get_sub_field('bwm_section_page_content_column2', $postID)))!!}
            @else()
              {!!get_sub_field('bwm_section_page_content_column2', $postID)!!}
            @endif
          </div>
        @endif
      </div>
    @endif
  @endwhile
@endif
