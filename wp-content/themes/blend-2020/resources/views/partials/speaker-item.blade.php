<a class="speakerItem" href="{{get_permalink($speaker->ID)}}">
  @if(get_post_thumbnail_id($speaker->ID))
    @php($crop = resize(get_post_thumbnail_id($speaker->ID), [300,300], true))
    <img
      class="speakerItem-img"
      src="{{$crop['src']}}"
      alt="{{$speaker->post_title}}"
      width="{{$crop['width']}}"
      height="{{$crop['height']}}">
  @endif
  <strong class="speakerItem-title">{{$speaker->post_title}}</strong>
  @if(get_field('speaker_job', $speaker))
    <span class="speakerItem-job">{{get_field('speaker_job', $speaker)}}</span>
  @endif
  @if(get_field('speaker_company', $speaker))
    <span class="speakerItem-company">{{get_field('speaker_company', $speaker)}}</span>
  @endif
</a>
