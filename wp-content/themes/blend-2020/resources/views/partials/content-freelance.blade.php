<article <?php post_class( 'freelance' ); ?>>
  @if( has_post_thumbnail() )
    @php($crop = resize(get_post_thumbnail_id(get_the_ID()), [90,90], true))
    <img
      src="{{$crop['src']}}"
      alt="{{get_the_title(get_the_ID())}}"
      width="{{$crop['width']}}"
      height="{{$crop['height']}}">
  @endif
  <?php
    $firstname = get_field('freelance_firstname', get_the_ID());
    $lastname = get_field('freelance_name', get_the_ID());
    $job = get_field('freelance_job', get_the_ID());
    $linkedin = get_field('freelance_linkedin', get_the_ID());
    $phone = get_field('freelance_phone', get_the_ID());
    $email = get_field('freelance_email', get_the_ID());
    $url = get_field('freelance_url', get_the_ID());
    $service = get_field('freelance_service', get_the_ID());
    $attendee = get_field('freelance_attendee', get_the_ID());
  ?>

  @if(is_array($service))
    @php($service = current($service))
  @endif

  @if(!empty($firstname) || !empty($lastname) || !empty($job))
    <h2 href="{{ get_the_permalink() }}" class="freelance__name">
      @if(!empty($firstname) || !empty($lastname))
        <strong>{{ $firstname }} {{ $lastname }}</strong>
      @endif
      @if(!empty($job))
        : {{$job}}
      @endif
    </h2>
  @endif

  @if(!empty($email))
    <a class="freelance__email" href="mailto:{{$email}}">{{$email}}</a>
  @endif

  @if(!empty($phone))
    <span class="freelance__phone">{{$phone}}</span>
  @endif

  @if(!empty($service))
    <span class="freelance__service freelance__service--{{ $service->slug }}">{{ $service->name }}</span>
  @endif

  @if( !empty($linkedin) || !empty($url) || $attendee )
    <ul class="freelance__meta">
      @if(!empty($linkedin))
        <li>
          <a href="{{$linkedin}}">
            <svg width="18" height="18" preserveAspectRatio="xMinYMin meet" focusable="false"
                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" xml:space="preserve">
                  <g class="bug-48dp" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <path
                      d="M0,4.00989318 C0,1.79529033 1.79405245,0 4.00989318,0 L43.9901068,0 C46.2047097,0 48,1.79405245 48,4.00989318 L48,43.9901068 C48,46.2047097 46.2059475,48 43.9901068,48 L4.00989318,48 C1.79529033,48 0,46.2059475 0,43.9901068 L0,4.00989318 Z M19,18.3 L25.5,18.3 L25.5,21.566 C26.437,19.688 28.838,18 32.445,18 C39.359,18 41,21.738 41,28.597 L41,41.3 L34,41.3 L34,30.159 C34,26.253 33.063,24.05 30.68,24.05 C27.375,24.05 26,26.425 26,30.159 L26,41.3 L19,41.3 L19,18.3 Z M7,41 L14,41 L14,18 L7,18 L7,41 Z M15,10.5 C15,12.985 12.985,15 10.5,15 C8.015,15 6,12.985 6,10.5 C6,8.015 8.015,6 10.5,6 C12.985,6 15,8.015 15,10.5 Z"
                      fill="#EC4F2B"></path>
                  </g>
                </svg>
            {{$linkedin}}
          </a>
        </li>
      @endif

      @if(!empty($url))
        <li class="freelance__url"><a href="{{$url}}">{{$url}}</a></li>
      @endif

      @if($attendee)
        <li class="freelance__attendee"><?php echo pll__( 'freelance.attendee' ); ?></li>
      @endif

    </ul>
  @endif
</article>
