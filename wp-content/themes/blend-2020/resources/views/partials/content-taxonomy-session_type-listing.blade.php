<section id="listing" class="listing-group">
  @foreach($sessionsByDay as $key => $sessions)
    @if($sessions)
      <h2 class="listing__day" id="{{ $days[$key]->slug }}">{{ $days[$key]->name }}</h2>
      <section class="singleSession-siblings-list sessionsList">
        @foreach($sessions as $session)
          @include('partials.session-item', ['session' => $session])
        @endforeach
      </section>
    @endif
  @endforeach
</section>
