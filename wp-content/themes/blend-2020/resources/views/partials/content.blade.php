<article @php(post_class())>
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}" data-namespace="post-template-default">{{ get_the_title() }}</a></h2>
  </header>
  <div class="entry-summary">
    @php(the_excerpt())
  </div>
</article>
