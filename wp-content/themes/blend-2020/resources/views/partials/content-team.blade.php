<article @php(post_class())>
  @if( has_post_thumbnail() )
    @php($crop = resize(get_post_thumbnail_id(get_the_ID()), [300,300], true))
    <img
      src="{{$crop['src']}}"
      alt="{{get_the_title()}}"
      width="{{$crop['width']}}"
      height="{{$crop['height']}}">
  @endif
  <a href="{{ get_the_permalink() }}" class="more-entry__title">{{ get_the_title() }}</a>
  @php( $terms = get_the_terms( get_the_ID(), 'team_job' ) )

  @if( $terms && ! is_wp_error( $terms ) )

    <?php
    $jobs_term = array();

    foreach ( $terms as $term ) {
      $jobs_term[] = $term->name;
    }

    $jobs = join( ", ", $jobs_term );
    ?>

    <span class="more-entry__job">
        {{ $jobs }}
      </span>
  @endif
</article>
