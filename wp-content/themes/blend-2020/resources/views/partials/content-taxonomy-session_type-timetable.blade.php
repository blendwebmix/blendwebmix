<div id="timelines" class="timelines">
  <nav>
    <button id="prevbtn" class="timeline__prev">{{ __('Voir les salles précedentes', 'bwm') }}</button>
    <button id="nextbtn" class="timeline__next">{{ __('Voir les salles suivantes', 'bwm') }}</button>
  </nav>
  <section class="timelines__container">
    @foreach($sessionsByDay as $key => $sessions)
      @if($sessions)
        <?php
        /**
         * List sessions by room foreach day
         */
        $sessionsByRoom = [];
        $rooms = [];
        $minHour = '23:59';
        $maxHour = '00:01';
        foreach ( $sessions as $session ) {
          $room           = get_field( 'session_room', $session );
          $sessionMinHour = explode( ' ', get_field( 'session_start', $session ) );
          $sessionMaxHour = explode( ' ', get_field( 'session_end', $session ) );

          if ( strtotime( $minHour ) > strtotime( $sessionMinHour[1] ) ) {
            $minHour = $sessionMinHour[1];
          }

          if ( strtotime( $maxHour ) < strtotime( $sessionMaxHour[1] ) ) {
            $maxHour = $sessionMaxHour[1];
          }

          /**
           * Add current $room->slug to $sessionsByRoom
           */
          if ( ! array_key_exists( $room->term_id, $sessionsByRoom ) ) {
            $sessionsByRoom[ $room->term_id ] = [];
            $rooms[ $room->term_id ]          = $room;
          }
          $sessionsByRoom[ $room->term_id ][] = $session;
        }

        usort($rooms, function($roomA, $roomB){
          return $roomA->slug > $roomB->slug;
        });
        // var_dump($minHour); // TODO: boucler entre minHour et maxHour pour générer .timeline_hours
        // var_dump($maxHour);
        ?>
        <section class="timeline">
          <h2 class="timeline__day" id="{{$days[$key]->slug}}">{{ $days[$key]->name }}</h2>
          <div class="timeline__container">
            <ul class="timeline__hours">
              <li class="timeline__hour">09:00</li>
              <li class="timeline__hour">10:00</li>
              <li class="timeline__hour">11:00</li>
              <li class="timeline__hour">12:00</li>
              <li class="timeline__hour">13:00</li>
              <li class="timeline__hour">14:00</li>
              <li class="timeline__hour">15:00</li>
              <li class="timeline__hour">16:00</li>
              <li class="timeline__hour">17:00</li>
              <li class="timeline__hour">18:00</li>
              <li class="timeline__hour">19:00</li>
              <li class="timeline__hour">20:00</li>
              <li class="timeline__hour">21:00</li>
            </ul>
            <ul class="timeline__rooms">
              @foreach($rooms as $room)
                <li class="timeline__room">
                  <h3 class="timeline__room-title">{{ $room->name }}</h3>
                  <ul class="timeline__events">
                    @foreach($sessionsByRoom[ $room->term_id ] as $session)
                      <?php
                      $theme = get_field( 'session_theme', $session->ID );
                      $speakers = get_field( 'session_speaker', $session->ID );
                      $level = intval( get_field( 'session_niveau', $session->ID ) );
                      $start = explode( ' ', get_field( 'session_start', $session->ID ) );
                      // $start[0] = Jour
                      // $start[1] = Heure
                      $end = explode( ' ', get_field( 'session_end', $session->ID ) );
                      // $end[0] = Jour
                      // $end[1] = Heure
                      ?>
                      <li class="timeline__event timeline__event--<?php echo $theme->slug; ?>"
                          data-start="{{ $start[1] }}"
                          data-end="{{ $end[1] }}">
                        <a href="<?php the_permalink( $session->ID ); ?>">
                          {{$session->post_title}}
                          <?php
                          $speakers_title = [];
                          $speakers_company = [];
                          if ( is_array( $speakers ) ) {
                            foreach ( $speakers as $speaker ) {
                              $speakers_title[]   = $speaker->post_title;
                              $speaker_company    = get_field( 'speaker_company', $speaker->ID );
                              $speakers_company[] = $speaker_company;
//                                      $speaker_job = get_field( 'speaker_job', $speaker->ID );
                            }
                          }
                          ?>
                          @if(is_array( $speakers ) && !empty($speakers))
                            <span class="timeline__speaker">
                              {{ sprintf( 'By %s @ %s', implode( ',', $speakers_title ), implode( ',', $speakers_company )) }}
                            </span>
                          @endif()
                          <span class="timeline__level">
                            @for ( $i = 0 ; $i<$level ; $i++)
                              <svg enable-background="new 0 0 454.534 454.534" version="1.1" viewBox="0 0 454.53 454.53" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
<path id="XMLID_1303_" d="m448.01 47.797-44.137-13.324c-2.533-0.765-5.277-0.39-7.521 1.013-2.243 1.403-3.775 3.703-4.2 6.314-6.702 41.179-19.042 72.54-27.15 89.937-28.099-9.925-58.994-4.815-84.689 27.648-11.573 14.621-58.154 130.56-114.49 155.38-61.425 27.066-121.23 13.823-153.57 2.385-3.768-1.333-7.964-0.076-10.379 3.108s-2.499 7.569-0.198 10.836c19.93 28.289 63.431 72.699 145.86 86.544 111.56 18.737 216.08-59.631 259.64-131.4 24.371-40.158 34.049-86.894 4.915-121.5 23.175-35.736 37.234-86.089 42.175-105.94 1.183-4.753-1.561-9.591-6.251-11.007z"/>
</svg>

                            @endfor
                          </span>
                        </a>
                      </li>
                    @endforeach
                  </ul>
                </li>
              @endforeach
            </ul>
          </div>
        </section>
      @endif
    @endforeach
  </section>
</div>
