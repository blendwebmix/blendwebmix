<section class="homepage-image-simple --{{ $position }}">
  @if(!empty($image))
    <div class="homepage-image-simple-image">
      @php( $img = wp_get_attachment_image( $image['ID'], 'medium', false, ['class' => 'homepage-image-simple-image__img']))
        {!! $img !!}
    </div>
  @endif
  <div class="homepage-image-simple-title">
    <h2 class="homepage-image-simple-title__title">
      {{ $title  }}
    </h2>

    @if( $subtitle)
      <div class="homepage-image-simple-title__subtitle">{!! $subtitle !!}</div>
    @endif

  </div>
</section>
