<article @php(post_class( ["more-entry"], $post->ID))>
  @if( has_post_thumbnail($post->ID) )
    @php($crop = resize(get_post_thumbnail_id(get_the_ID()), [300,300], true))
    <img
      src="{{$crop['src']}}"
      alt="{{get_the_title()}}"
      width="{{$crop['width']}}"
      height="{{$crop['height']}}">
  @endif
  <a href="{{ get_the_permalink( $post->ID ) }}" class="more-entry__title">{{ get_the_title( $post->ID ) }}</a>
</article>
