<article class="container entry-post">
  <section class="entry-post__content">
    <h1>{!! the_title() !!}</h1>
    <div class="singleSpeaker-job-company">
      @if(get_field('speaker_job', get_the_ID()))
        <span class="singleSpeaker-job">{{get_field('speaker_job', get_the_ID())}}</span>
      @endif
      @if(get_field('speaker_company', get_the_ID()))
        <span class="singleSpeaker-company">{{get_field('speaker_company', get_the_ID())}}</span>
      @endif
    </div>
    <a class="singleSpeaker-back-button" href='{{ get_post_type_archive_link('speaker') }}'>
      <span>Retour à la liste des speakers</span>
    </a>
    {{ the_content() }}
    <?php
    $sessions = get_field( 'session_speaker' );
    ?>
    @if(is_array($sessions))
      <h2 class="singleSpeaker-meet">{{ sprintf( pll__('speaker.single.meet'), get_the_title() ) }}</h2>

      <nav class="singleSpeaker-siblings-list sessionsList">
        @foreach($sessions as $session)
          @include('partials.session-item', ['session' => $session])
        @endforeach
      </nav>
    @endif
  </section>
</article>
