<?php
// Get all sponsor_type with non-empty sponsors
$sponsor_types = get_terms( [
  'taxonomy'   => 'sponsor_type',
  'hide_empty' => true,
  'orderby'    => 'slug'
] );
?>

<footer class="footer">
  @foreach( $sponsor_types as $type)
    <section class="sponsors sponsors__{{ $type->slug }}">
      <strong class="sponsors-title">{{ $type->name }}</strong>

      <?php
      // TODO: C'est pas l'idéal mais à défaut de mieux pour le moment
      // Get all sponsors related to current sponsor_type
      $sponsors = get_posts( [
        'post_type'      => 'sponsor',
        'orderby'        => 'name',
        'posts_per_page' => - 1,
        'tax_query'      => [
          [
            'taxonomy' => 'sponsor_type',
            'field'    => 'slug',
            'terms'    => $type->slug,
            'operator' => 'IN'
          ],
        ],
      ] );
      ?>
      @if( count($sponsors) > 0 )
        <ul class="sponsors-list">
          @foreach( $sponsors as $sponsor)
            @if( has_post_thumbnail($sponsor->ID) )
              <li class="sponsor"><a href="{!! get_the_permalink( $sponsor->ID ) !!}">
                  @php($crop = resize(get_post_thumbnail_id($sponsor->ID), [300,300], false))
                  <img
                    src="{{$crop['src']}}"
                    alt="{{$sponsor->post_title}}"
                    width="{{$crop['width']}}"
                    height="{{$crop['height']}}">
                </a></li>
            @endif
          @endforeach
        </ul>
      @endif
    </section>
  @endforeach
  <section class="nav-footer">
      <div class="nav-footer-content wp-block-columns">
        <div class="footer-newsletter wp-block-column">

            <?php
              $currentlang = get_bloginfo('language');
              // si la langue afficher est le français
              if($currentlang=="fr-FR"):echo do_shortcode('[sibwp_form id=1]');
              // si la langue afficher est l anglais
              elseif($currentlang=="en-GB"):echo do_shortcode('[sibwp_form id=2]');
              endif;
            ?>

        </div>

        <div class="wp-block-column">
          <?php
          $sticky = get_option( 'sticky_posts' );
          $args = array(
            'post__in'       => $sticky,
            'posts_per_page' => 1
          );
          $posts = get_posts( $args );

          ?>

          @if( count($posts) > 0)
            @php( $post = current($posts) )
            <article class="footer-post">
              <?php echo get_the_post_thumbnail($post->ID, 'large', ['class' => 'footer-post__thumb']); ?>
                <a class="footer-post__title" href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
            </article>
          @endif
          <figure class="nav-footer__logo">
            <img src="@asset('images/logo-desktop.svg')" width="60" height="51"
                alt="Logo BlendWebMix <?php echo date( 'Y' ); ?>">
            &copy; <?php echo date( 'Y' ); ?>
            @php( $url = get_post_type_archive_link('team') )
            <figcaption><?php echo sprintf( __( 'Made by love by <a href="%s">awesome volunteers!</a>', 'bwm' ), $url ); ?></figcaption>
          </figure>
          @if ( has_nav_menu('footer_navigation') )
            {!! wp_nav_menu([
              'theme_location' => 'footer_navigation',
              'menu_class' => 'nav',
              'container' => false,
            ]) !!}
          @endif
        </div>
      </div>
  </section>
  @if ( has_nav_menu('historical_navigation') )
    {!! wp_nav_menu([
      'theme_location' => 'historical_navigation',
      'menu_class' => 'nav',
      'container' => 'nav',
      'container_class' => 'nav-historical',
    ]) !!}
  @endif
</footer>
