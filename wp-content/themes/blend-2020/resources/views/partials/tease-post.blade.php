<article @php(post_class( ["more-entry"], $post->ID))>
  @if( has_post_thumbnail($post->ID) )
    @php($crop = resize(get_post_thumbnail_id($post->ID), [300,null], true))
    <img
      src="{{$crop['src']}}"
      alt="{{$post->post_title}}"
      width="{{$crop['width']}}"
      height="{{$crop['height']}}">
  @endif
  <a href="{{ get_the_permalink( $post->ID ) }}" class="more-entry__title">{!! get_the_title( $post->ID ) !!}</a>
</article>
