<main class="entry-page">
  @if(!get_field('cover'))
    <h1 class="entry-page__title">{{get_the_title()}}</h1>
  @endif
  @php(the_content())
</main>

