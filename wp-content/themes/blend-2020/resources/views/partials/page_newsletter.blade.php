<section class="page-newsletter">
    @if( $message)
        <h2 class="page-newsletter__title">{{ $title }}</h2>
        <P class="page-newsletter__subtitle">{{ $subtitle }}</P>
        <DIV class="page-newsletter__message">{!! $message !!}</DIV>
    @endif
</section>
