<article {{post_class('sessionItem', $session->ID)}}>
  <span class="sessionItem-metas">
    <span class="sessionItem-theme sessionTheme sessionTheme--{{get_field('session_theme', $session->ID)->slug}}">
      {{get_field('session_theme', $session->ID)->name}}
    </span>
  </span>


  @php
    $start = new DateTime(get_field('session_start', $session->ID));
    $end = new DateTime(get_field('session_end', $session->ID));
  @endphp
  <section class="sessionItem-content">
    <a href="{{get_permalink($session->ID)}}" class="sessionItem-title">{{$session->post_title}}</a>
    @if(get_field('session_speaker', $session->ID))
      <span class="sessionItem-speakers">par
        @foreach(get_field('session_speaker', $session->ID) as $speaker)
          <a href="{{get_permalink($speaker->ID)}}">
            {{$speaker->post_title}}
            @if(get_field('speaker_company', $speaker))
              ({{get_field('speaker_company', $speaker)}})
            @endif
          </a>
        @endforeach
      </span>
    @endif
    <span class="sessionItem-date">
    @if(get_field('session_day', $session->ID)->name)
        <span class="sessionItem-date-day">
        @include('partials.icon', ['icon' => 'calendar'])
          {{ get_field('session_day', $session->ID)->name }}
      </span>
    @endif
    <span class="sessionItem-date-time">
      @include('partials.icon', ['icon' => 'hour'])
      {{ $start->format('G\hi') . ' - ' . $end->format('G\hi') }}
    </span>

    @if(get_field('session_room', $session->ID)->name)
        <span class="sessionItem-room">
      <span>{{get_field('session_room', $session->ID)->name}}</span>
    </span>
      @endif
  </span>
  </section>

</article>
