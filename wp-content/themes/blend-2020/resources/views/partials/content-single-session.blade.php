<article class="singleSession content entry-session">
  <header class="singleSession-header">
      <span class="singleSession-theme sessionTheme sessionTheme--{{get_field('session_theme', $session)->slug}}">
        {{get_field('session_theme')->name}}
      </span>

    <h1 class="singleSession-title">{{the_title()}}</h1>

    <?php
    $start = new DateTime( get_field( 'session_start' ) );
    $end = new DateTime( get_field( 'session_end' ) );
    ?>

    <p class="singleSession-metas">
        <span class="singleSession-metas-item singleSession-metas-item--day">
          @include('partials.icon', ['icon' => 'calendar'])
          <strong>{{ get_field('session_day')->name }}</strong>
        </span>
      <span class="singleSession-metas-item singleSession-metas-item--time">
          @include('partials.icon', ['icon' => 'hour'])
        <strong>{{ $start->format('G:i') . ' → ' . $end->format('G:i') }}</strong>
        </span>
      <span class="singleSession-metas-item singleSession-metas-item--room">
          @include('partials.icon', ['icon' => 'room'])
        <strong>{{ get_field('session_room')->name }}</strong>
        </span>
    </p>
  </header>

  <main class="singleSession-content">
    {{ the_content() }}

    <nav class="singleSession-speakers speakersList">
      @foreach(get_field('session_speaker') as $speaker)
        @include('partials.speaker-item', ['speaker' => $speaker])
      @endforeach
    </nav>
  </main>

  <?php
  $sessions = get_posts( [
    'post_type'      => 'session',
    'posts_per_page' => 4,
    'orderby'        => 'menu_order',
    'order'          => 'ASC',
    'post__not_in'   => [ get_the_ID() ],
    'tax_query'      => [
      [
        'taxonomy' => 'session_theme',
        'field'    => 'term_id',
        'terms'    => get_field( 'session_theme' )->term_id,
      ]
    ]
  ] );
  ?>

  @if(!empty($sessions))
    <aside class="singleSession-siblings">
      <h2 class="singleSession-siblings-title">
        {{pll__('session.single.more')}} {{get_field('session_theme')->name}}
      </h2>

      <nav class="singleSession-siblings-list sessionsList">
        @foreach($sessions as $session)
          @include('partials.session-item', ['session' => $session])
        @endforeach
      </nav>
    </aside>
  @endif
</article>
