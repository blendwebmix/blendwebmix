<article @php(post_class())>
  @if( has_post_thumbnail() )
    @php($crop = resize(get_post_thumbnail_id(get_the_ID()), [300,300], true))
    <img
      src="{{$crop['src']}}"
      alt="{{get_the_title()}}"
      width="{{$crop['width']}}"
      height="{{$crop['height']}}">
  @endif
  <a href="{{ get_the_permalink() }}" class="more-entry__title">{{ get_the_title() }}</a>
  @php( $job = get_field( 'speaker_job', get_the_ID() ) )
  @php( $company = get_field( 'speaker_company', get_the_ID() ) )

  @if( !empty($job) )
    <span class="more-entry__job">
      {{ $job }}
      @if( !empty($company) )
        <span class="more-entry__company">@ {{ $company }}</span>
      @endif
    </span>
  @endif
</article>
