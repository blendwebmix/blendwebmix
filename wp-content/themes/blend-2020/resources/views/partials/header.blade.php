<a class="skip-link screen-reader-text" href="#start-content">{{ pll__('skip-content', 'bwm') }}</a>

<header class="header">

  <a href="<?php echo site_url(); ?>" class="logo">
    <img src="@asset('images/logo-mobile.svg')" alt="Logo BlendWebMix" class="mobile" width="156" height="63">
    <img src="@asset('images/logo-desktop.svg')" alt="Logo BlendWebMix" class="desktop" width="314" height="72">
  </a>

  <?php
  // Get page "sponsor"
  $args = array(
    'name'        => 'deviens-sponsor',
    'post_type'   => 'page',
    'post_status' => 'publish',
    'numberposts' => 1,
  );
  $sponsor = get_posts( $args );

  // Get page "programme"
  $args = array(
    'name'        => 'programme',
    'post_type'   => 'page',
    'post_status' => 'publish',
    'numberposts' => 1,
  );
  $program = get_posts( $args );

  // Get page "billeterie"
  $args = array(
    'name'        => 'billetterie',
    'post_type'   => 'page',
    'post_status' => 'publish',
    'numberposts' => 1,
  );
  $ticketing = get_posts( $args );
  ?>

<!-- Get page "program" -->
@if( !empty( $program ) )
    @php
      $program_id = current($program)->ID;
      if( function_exists('pll_get_post')){
        $program_id = pll_get_post( $program_id );
      }
    @endphp
    <a class="header__btn header__btn--session" href="{{ get_the_permalink( $program_id ) }}">{{ pll__('Sessions', 'bwm') }}</a>
  @endif

<!-- Get page "sponsor" -->
  @if( !empty( $sponsor ) )
    @php
      pll_register_string( 'bwm', 'Sponsor', 'bwm', false );
      $sponsor_id = current($sponsor)->ID;
      if( function_exists('pll_get_post') ){
        $sponsor_id = pll_get_post( $sponsor_id );
      }
    @endphp
    <a class="header__btn header__btn--sponsor" href="{{ get_the_permalink( $sponsor_id ) }}">{{ pll__('Sponsor', 'bwm') }}</a>
  @endif

  <!-- Get page tiketing -->
  @if( !empty( $ticketing ) )
    @php
      $ticketing_id = current($ticketing)->ID;
      if( function_exists('pll_get_post') ){
        $ticketing_id = pll_get_post( $ticketing_id );
      }
    @endphp
    <a class="header__btn header__btn--ticketing" href="{{ get_the_permalink( $ticketing_id ) }}">{{ pll__('Ticketing', 'bwm') }}</a>
  @endif

  <button class="menu-toggle"><span class="menu-toggle__hamburger"></span> <span
      class="menu-toggle__text">{{ __('Menu', 'bwm') }}</span></button>

  @if ( has_nav_menu('primary_navigation') )
    {!! wp_nav_menu([
      'theme_location' => 'primary_navigation',
      'menu_class' => 'nav',
      'container' => 'nav',
      'container_class' => 'nav-primary',
    ]) !!}
  @endif

  @if ( has_nav_menu('social_navigation') )
    {!! wp_nav_menu([
      'theme_location' => 'social_navigation',
      'menu_class' => 'nav',
      'container' => 'nav',
      'container_class' => 'nav-social-network',
    ]) !!}
  @endif

  @if ( has_nav_menu('social_navigation') )
    {!! wp_nav_menu([
      'theme_location' => 'language_accessibilty_navigation',
      'menu_class' => 'nav',
      'container' => 'nav',
      'container_class' => 'nav-language-accessibilty',
    ]) !!}
  @endif

</header>
