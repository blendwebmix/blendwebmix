<section class="blog-extract more">
  <h2 class="blog-extract__title">{{ $title }}</h2>
  <a href="{{ get_permalink( get_option( 'page_for_posts' ) ) }}" class="blog-extract__link">@php(_e('Go to blog', 'bwm'))</a>
  @foreach($posts as $post)
    @include('partials/tease-post', $posts)
  @endforeach
</section>
