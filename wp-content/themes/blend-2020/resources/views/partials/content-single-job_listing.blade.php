<article class="container entry-single">

  <div class="entry-post">

    <div class="entry-post_content">
      @php( the_content() )
    </div>

    <?php // Social sharing icons (DSIGNED 180409)
    $share_url = wp_get_canonical_url();
    $share_title = rawurlencode( get_the_title() ) ;
    $share_popup = 'onclick="window.open(this.href,this.title,\'width=500,height=500,top=300px,left=300px\');  return false;"' ;
    ?>
    <div class="social-share">

      <div id="fb-root"></div>
      <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/fr_CA/sdk.js#xfbml=1&version=v2.12&appId=611743255624230&autoLogAppEvents=1';
          fjs.parentNode.insertBefore(js, fjs);
        }
        (document, 'script', 'facebook-jssdk'));
      </script>
      <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>

      <span class="social-share__title"><?php _e('Share this!','bwm'); ?></span>

      <a class="social-share__button social-share__button--facebook" href="https://www.facebook.com/sharer.php?u=<?php echo $share_url; ?>" title="<?php _e('Share on Facebook','bwm'); ?>" <?php echo $share_popup; ?> target="_blank"><i class="icon icon-facebook"></i></a>
      <a class="social-share__button social-share__button--linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $share_url; ?>&title=<?php echo $share_title; ?>" title="<?php _e('Share on LinkedIn','bwm'); ?>" <?php echo $share_popup; ?> target="_blank"><i class="icon icon-linkedin"></i></a>
      <a class="social-share__button social-share__button--twitter" href="https://twitter.com/share?url=<?php echo $share_url ?>" title="<?php _e('Share on Twitter','bwm'); ?>" <?php echo $share_popup; ?> target="_blank"><i class="icon icon-twitter"></i></a>

    </div>

  </div>

</article>

<?php
$posts = get_posts([
  'post_type'      => 'job_listing',
  'orderby'       => 'rand',
  'posts_per_page' => 6,
  'post__not_in'   => array( get_the_ID() )
]);
?>

@if( !empty($posts) )
  <section class="more">
    <span class="more-title">@php( _e('Stay here, other items are to discover!', 'bwm') )</span>
    @foreach( $posts as $post )
      @include( 'partials.tease-post', array('post' => $post) )
    @endforeach
  </section>
@endif
