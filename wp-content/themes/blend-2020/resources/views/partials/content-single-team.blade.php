<article {{ post_class('entry-team') }}>
  <h1 class="entry-team__title">{{ get_the_title() }}</h1>
  @php( $terms = get_the_terms( get_the_ID(), 'team_job' ) )

  @if( $terms && ! is_wp_error( $terms ) )

    <?php
      $jobs_term = array();

      foreach ( $terms as $term ) {
      $jobs_term[] = $term->name;
      }

      $jobs = join( ", ", $jobs_term );
    ?>

    <span class="entry-team__job">
      {{ $jobs }}
    </span>
  @endif

  @if( has_post_thumbnail() )
    @php($crop = resize(get_post_thumbnail_id(get_the_ID()), [1024,null], true))
    <img
      src="{{$crop['src']}}"
      alt="{{get_the_title()}}"
      width="{{$crop['width']}}"
      height="{{$crop['height']}}">
  @endif

  <div class="entry-team__content">
    @php( the_content() )
  </div>
</article>

<?php
$posts = get_posts([
  'post_type'      => 'team',
  'orderby'       => 'rand',
  'posts_per_page' => 12,
  'post__not_in'   => array( get_the_ID() )
]);
?>

@if( !empty($posts) )
  <section class="more">
    <span class="more-title">@php( _e('Stay here, other items are to discover!', 'bwm') )</span>
    @foreach( $posts as $post )
      @include( 'partials.tease-team', array('post' => $post) )
    @endforeach
  </section>
@endif
