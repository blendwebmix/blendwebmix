<article @php(post_class())>
  @if( has_post_thumbnail() )
    @php( $crop = resize( get_post_thumbnail_id(get_the_ID()), [ 300, null ], true ) )
    <img
      src="{{ $crop['src'] }}"
      alt="{{ get_the_title() }}"
      width="{{ $crop['width'] }}"
      height="{{ $crop['height'] }}">
  @endif
  <time pubdate="{{ get_the_date() }}" class="post-date">{{ get_the_date() }}</time>
  <h2 class="post-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
</article>
