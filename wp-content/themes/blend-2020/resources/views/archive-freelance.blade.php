@extends('layouts.base')

@section('content')

  <main class="entry-archive">

    <div class="archive-content">

      <?php
      // Get page with same slug that current taxonomy
      $freelances_page = get_posts( [
        'name'        => get_queried_object()->has_archive,
        'post_type'   => 'page',
        'post_status' => 'publish',
        'numberposts' => 1,
      ] );

      // Get translation of this page
      if ( ! empty( $freelances_page ) ) :
        $page    = current( $freelances_page );
        $post_id = pll_get_post( $page->ID );
        $post    = get_post( $post_id );
      endif;
      ?>

      @if( !empty( $post ) )

        <!-- <section class="page-content">
          @include('partials/content-flexible', ['postID' => $post->ID])
        </section> -->

      @endif


      <?php
      $terms = get_terms( array(
        'taxonomy'   => 'freelance_service',
        'parent'     => 0,
        'hide_empty' => false
      ) );
      ?>

      @if(is_array($terms) && !empty($terms))
        <ul class="freelance-services">
          <li class="freelance-services__item">Tous les thèmes</li>
          @foreach($terms as $service)
            <li class="freelance-services__item freelance-services__item--{{ $service->slug }}">
              <a href="{{ get_term_link($service) }}">{{ $service->name }}</a>
            </li>
          @endforeach()
        </ul>
      @endif()

      @while (have_posts()) @php(the_post())
      @include ('partials.content-'.( get_post_type() !== 'post' ? get_post_type() : get_post_format() ))
      @endwhile

      {!! get_the_posts_navigation() !!}

    </div>

  </main>

@endsection
