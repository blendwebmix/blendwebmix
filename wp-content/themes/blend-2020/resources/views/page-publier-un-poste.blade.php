@extends('layouts.base')

@section('content')
  @while( have_posts() ) @php( the_post() )
  <main class="entry-home">
    @include('partials/content-flexible', ['postID' => get_the_ID(), 'wpautop' => false])
  </main>
  @endwhile
@endsection
