@extends('layouts.base')

@section('content')

  <main class="entry-archive">

    <div class="archive-content">

      @php($terms = get_terms( array(
        'taxonomy' => 'freelance_service',
        'parent' => 0,
        'hide_empty' => false
      ) ))

      @php($current = get_queried_object())

      @if(is_array($terms) && !empty($terms))
        <ul class="freelance-services">
          <li class="freelance-services__item freelance-services__item--all"><a href="{{ get_post_type_archive_link( 'freelance' ) }}">Tous les thèmes</a></li>
          @foreach($terms as $service)
            <li class="freelance-services__item freelance-services__item--{{ $service->slug }} {{ $service->slug == $current->slug ? 'freelance-services__item--current' : '' }}">
              @if($service->slug !== $current->slug)
                <a href="{{ get_term_link($service) }}">{{ $service->name }}</a>
              @else()
                {{ $service->name }}
              @endif()
            </li>
          @endforeach()
        </ul>
      @endif()

      @while (have_posts()) @php(the_post())
      @include ('partials.content-'.( get_post_type() !== 'post' ? get_post_type() : get_post_format() ))
      @endwhile

      {!! get_the_posts_navigation() !!}

    </div>

  </main>

@endsection
