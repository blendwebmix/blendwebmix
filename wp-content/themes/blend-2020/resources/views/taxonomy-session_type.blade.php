@extends('layouts.base')

@section('content')

  <main class="entry-archive">

    <?php
    // Get current archive
    $queried_object = get_queried_object();

    $args = array(
      'name'        => $queried_object->slug,
      'post_type'   => 'page',
      'post_status' => 'publish',
      'numberposts' => 1,
    );

    $page = get_posts( $args );

    if ( ! empty( $page ) ) :
      $page    = current( $page );
      $post_id = $page->ID;
      if ( function_exists( 'pll_get_post' ) ) {
        $post_id = pll_get_post( $page->ID );
      }
      $post = get_post( $post_id );

      if ( ! empty( $post ) ): ?>

        <?php setup_postdata( $post ); ?>
        <?php echo $post->post_content; ?>
        <?php wp_reset_postdata(); ?>

      <?php endif; ?>
	  <?php endif; ?>

    <section class="sessions-timeline">

      @php
        // Get page with same slug that current taxonomy
        $tax_page = get_posts([
          'name'        => get_queried_object()->slug,
          'post_type'   => 'page',
          'post_status' => 'publish',
          'numberposts' => 1,
        ]);

        // Get translation of this page
        if( ! empty( $tax_page ) ) :
          $post_id = pll_get_post($tax_page[0]->ID);
          $page = get_post($post_id);
        endif;
      @endphp

      @if( ! empty($page) )
        <article class="archiveSession-content">
          @include('partials/content-flexible', ['postID' => $page->ID])
        </article>
      @endif

      @php
        // Get all sessions in this taxonomy, grouped by day
      $days = [];
      $sessionsByDay = [];
            foreach(get_terms('session_day', ['orderby' => 'slug']) as $day) {
            $days[] = $day;
              $sessionsByDay[] = get_posts([
                'post_type' => 'session',
                'posts_per_page' => -1,
                'meta_key' => 'session_start',
                'orderby' => 'meta_value',
                'order' => 'ASC',
                'tax_query' => [
                  [
                  'taxonomy' => 'session_day',
                  'field' => 'term_id',
                  'terms' => $day->term_id,
                  ],
                  [
                    'taxonomy' => 'session_type',
                    'field' => 'term_id',
                    'terms' => get_queried_object()->term_id
                  ]
                ]
              ]);
            }

      $themes = get_terms('session_theme');
      @endphp

      @if($days)
        <nav class="switch-group">
          <ul class="switch-to">
            @foreach($days as $day)
              <li class="switch-to__day">
                <a class="switch-to__day-link" href="#{{$day->slug}}">{{$day->name}}</a>
              </li>
            @endforeach()
          </ul>

          <ul class="switch-to" id="switch-view-events">
            <li class="switch-to__view @if (!get_query_var('event_listing')) switch-to__view--active @endif">
              <button class="switch-to__view-link" data-show="#timelines">
                <?php _e( 'Schedule', 'bwm' ); ?>
              </button>
            </li>
            <li class="switch-to__view  @if (get_query_var('event_listing')) switch-to__view--active @endif">
              <button class="switch-to__view-link" data-show="#listing">
                <?php _e( 'Listing', 'bwm' ); ?>
              </button>
            </li>
          </ul>
        </nav>
        <ul class="filter-by">
          <li class="filter-by__item filter-by__item--all">
            <button data-filter="all"><?php _e( 'All themes', 'bwm' ); ?></button>
          </li>
          @foreach($themes as $theme)
            <li class="filter-by__item filter-by__item--<?php echo $theme->slug ?>">
              <button data-filter="<?php echo $theme->slug ?>"><?php echo $theme->name ?></button>
            </li>
          @endforeach()
        </ul>

        @include ('partials.content-taxonomy-session_type-timetable')
      </section>

        @include ('partials.content-taxonomy-session_type-listing')
    @endif

    </section>

  </main>
@endsection
