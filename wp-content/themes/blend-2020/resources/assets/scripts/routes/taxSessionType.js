import Schedule from './../util/Schedule'

export default {

  addActive(el) {
    el.parentNode.classList.add(`${el.parentNode.classList[0]}--active`);
  },

  removeActive(el) {
    el.parentNode.classList.remove(`${el.parentNode.classList[0]}--active`);
  },

  displayView(item) {

    if( ! item ){
      return;
    }

    let selector = item.getAttribute('data-show');
    let el = document.querySelector(selector);

    if( ! el ){
      return;
    }

    let classname = el.classList[0];
    if (item.parentNode.classList.contains('switch-to__view--active')) {
      el.classList.remove(`${classname}--hide`);
    } else {
      el.classList.add(`${classname}--hide`);
    }
  },

  hideEvents(events) {
    Array.from(events).forEach(el => {
      let classname = el.classList[0];
      el.classList.remove(`${classname}--hide`);
      el.classList.add(`${classname}--hide`);
    })
  },

  showEvents(events) {
    Array.from(events).forEach(el => {
      let classname = el.classList[0];
      el.classList.add(`${classname}--hide`);
      el.classList.remove(`${classname}--hide`);
    })
  },

  onClickFilterHandle(e) {
    let thisClassname = e.target.getAttribute('data-filter');

    /**
     * Prevent themes are active when all is active
     */
    if (thisClassname === 'all') {
      this.filters.forEach(el => {
        el.classList.remove('active');
      })
    } else {
      this.filters[0].classList.remove('active');
    }

    e.target.classList.toggle('active');
    let filtersActive = document.querySelectorAll('[data-filter].active');

    /**
     * If no filter are active then active "All"
     */
    if (filtersActive.length <= 0 || !this.filters[0].classList.contains('active') && filtersActive.length === this.filters.length - 1) {
      this.filters.forEach(el => el.classList.remove('active'))
      this.filters[0].classList.add('active');
      filtersActive = [this.filters[0]];
    }

    /**
     * Remove all visible events
     */
    this.hideEvents(this.timelineEvents);
    this.hideEvents(this.listingEvents);

    /**
     * Activate events by filter
     */
    Array.from(filtersActive).forEach(filterActive => {
      let filterClassname = filterActive.getAttribute('data-filter');

      if (filterClassname !== 'all') {
        let timelineEventsVisible = Array.from(this.timelineEvents).filter(el => el.classList.contains(`timeline__event--${filterClassname}`));
        let listingEventsVisible = Array.from(this.listingEvents).filter(el => el.classList.contains(`session_theme-${filterClassname}`));

        this.showEvents(timelineEventsVisible);
        this.showEvents(listingEventsVisible);
      } else {
        this.showEvents(this.timelineEvents);
        this.showEvents(this.listingEvents);
      }
    })
  },

  onClickViewHandle(e) {
    e.preventDefault();

    this.buttonsViewListing.forEach(btn => {
      this.removeActive(btn);
      this.displayView(btn);
    });

    this.addActive(e.target);
    this.displayView(e.target)
  },

  //Affichage de la liste d'événements en listing // Appel AJAX
  // displayListingWithAjax() {
  //   var myData = {
  //     action  :   'events_listing',
  //     slug    :   BlendLink.slug,
  //     termId  :   BlendLink.term_id
  //   }
  //
  //   $.ajax({
  //     type: "POST",
  //     url: BlendLink.ajaxurl,
  //     data: myData,
  //     success: function(data){
  //       $('.sessions-timeline').append(data);
  //     }
  //   });
  // },

  init() {
    this.filters = document.querySelectorAll('[data-filter]');
    this.timelines = document.querySelector('.timelines__container');

    if ( ! this.timelines ) {
      return;
    }

    this.timelineNumber = this.timelines.childElementCount;
    this.timelineEvents = document.querySelectorAll('.timeline__event');
    this.listingEvents = document.querySelectorAll('.sessionItem');
    const timeline = this.timelines.querySelectorAll('.timeline');
    const dayLinks = document.querySelectorAll('.switch-to__day-link');

    if ( ! timeline.length || ! dayLinks.length ) {
      return;
    }

    new Schedule('.timeline');
    //Tests pour voir sur quel affichage on est : listing / timeline
    // if (timeline.length) {
    //   this.initTimeline();
    //   this.isTimeline = true;
    // } else if (document.querySelectorAll('.listing').length) {
    //   this.initListing();
    //   this.isListing = true;
    // }

    if (window.innerWidth > 768) {
      this.timelines.style.width = `${this.timelineNumber * 100}%`;
    } else {
      this.timelines.removeAttribute('style');
    }

    this.addActive(dayLinks[0]);
    timeline[0].classList.add('timeline--active');

    /**
     * On click add the good transform to show events of active day
     */
    if (window.innerWidth > 768) {
      dayLinks.forEach(links => {

        if (!links) {
          return;
        }

        links.addEventListener('click', event => {
          event.preventDefault();

          dayLinks.forEach(link => {
            this.removeActive(link);
          });

          this.addActive(event.target);
          const clickedElement = Array.prototype.indexOf.call(dayLinks, event.target);

          Array.from(timeline).forEach(t => {
            t.classList.remove('timeline--active');
          });

          timeline[clickedElement].classList.add('timeline--active');
          const percent = 100 / this.timelineNumber;

          if (window.innerWidth > 768) {
            this.timelines.style.transform = `translateX(${-(percent * clickedElement)}%)`;
          } else {
            this.timelines.removeAttribute('style')
          }
        })
      });
    }

    this.filters[0].classList.toggle('active');
    this.filters.forEach(filter => {
      filter.addEventListener('click', this.onClickFilterHandle.bind(this));
    });

    //View listing
    this.buttonsViewListing = document.querySelectorAll('.switch-to__view-link');
    this.displayView = this.displayView.bind(this);

    this.buttonsViewListing.forEach(item => {
      this.displayView(item);
      item.addEventListener('click', this.onClickViewHandle.bind(this));
    });

    // CTA SLIDE
    const next = document.getElementById('nextbtn');
    const prev = document.getElementById('prevbtn');

    if (!next) {
      return;
    }
    if (!prev) {
      return;
    }

    function sideScroll(element, direction, speed, distance, step) {
      let scrollAmount;
      scrollAmount = 0;
      const slideTimer = setInterval(function () {
        if (direction == 'left') {
          element.scrollLeft -= step;
        } else {
          element.scrollLeft += step;
        }
        scrollAmount += step;
        if (scrollAmount >= distance) {
          window.clearInterval(slideTimer);
        }
      }, speed);
    }

    next.addEventListener('click', () => {
      const container = document.querySelector('.timeline--active');
      sideScroll(container, 'right', 25, 300, 30);
    });

    prev.addEventListener('click', () => {
      const container = document.querySelector('.timeline--active');
      sideScroll(container, 'left', 25, 300, 30);
    });
  },

  finalize() {

  },
}
