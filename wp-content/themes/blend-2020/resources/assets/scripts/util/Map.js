import $ from 'jquery'
import GoogleMapsLoader from 'google-maps'

export default class Map {
	constructor() {
		this.zoom = 15

		GoogleMapsLoader.KEY = 'AIzaSyBvHpnxCyyjit6jqmf_3OTUQzK2vQvnIYw'

		GoogleMapsLoader.load((google) => {
			this.google = google

			this.loaded()
		})
	}

	loaded() {
		this.maps = []

		$('.acf-map').each((i, el) => {
			this.maps.push(this.new_map($(el)))
		})
	}

	new_map($el) {
		const $markers = $el.find('.marker'),
			map = new this.google.maps.Map($el[0], {
				zoom: this.zoom,
				center: new this.google.maps.LatLng(0, 0),
				mapTypeId: this.google.maps.MapTypeId.ROADMAP,
				// styles: null
			})

		map.markers = []

		$markers.each((i, el) => {
			this.add_marker($(el), map)
		})

		this.center_map(map)

		return map
	}

	add_marker($marker, map) {
		const latlng = new this.google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng')),
			marker = new this.google.maps.Marker({
				position: latlng,
				map,
			})

		map.markers.push(marker)

		// if marker contains HTML, add it to an infoWindow
		if ($marker.html()) {
			const infowindow = new this.google.maps.InfoWindow({
				content: $marker.html(),
			})

			this.google.maps.event.addListener(marker, 'click', () => {
				infowindow.open(map, marker)
			})
		}
	}

	center_map(map) {
		const bounds = new this.google.maps.LatLngBounds()

		$.each(map.markers, (i, marker) => {
			const latlng = new this.google.maps.LatLng(marker.position.lat(), marker.position.lng())

			bounds.extend(latlng)
		})

		if (map.markers.length == 1) {
			map.setCenter(bounds.getCenter())
			map.setZoom(this.zoom)
		} else map.fitBounds(bounds)
	}
}
