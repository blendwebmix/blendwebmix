// Schedule JS
export default class Schedule {
  constructor(selector) {
    let timelines = document.querySelectorAll(selector);

    if (!timelines) {
      throw new Error(`The selector ${selector} didn't found any elements`)
    }

    timelines.forEach(timeline => {
      new ScheduleElement(timeline)
    })
  }
}

class ScheduleElement {

  constructor(element) {
    this.timelineHourElement = element.querySelectorAll('.timeline__hour');

    this.firstHourElement = this.timelineHourElement[0];
    this.timelineStart = this.getScheduleTimestamp(this.firstHourElement.textContent);
    this.timelineUnitDuration = this.getScheduleTimestamp(this.timelineHourElement[1].textContent) - this.timelineStart;

    this.singleEvents = document.querySelectorAll('.timeline__event');

    this.computedStyleEvent = window.getComputedStyle(this.singleEvents[0]);
    this.computedStyleTop = parseInt(this.computedStyleEvent.getPropertyValue('margin-top').replace('px', ''), 10);
    this.computedStyleBottom = parseInt(this.computedStyleEvent.getPropertyValue('margin-bottom').replace('px', ''), 10);
    let computedHourAfter = window.getComputedStyle(this.timelineHourElement[0], '::after')
    this.hourAfterTop = parseInt(computedHourAfter.getPropertyValue('top').replace('px', ''), 10);
    this.hourAfterHeight = parseInt(computedHourAfter.getPropertyValue('height').replace('px', ''), 10);

    this.isInit = false;

    this.placeEvents = this.placeEvents.bind(this);
    this.reset = this.reset.bind(this);
    this.handle = this.handle.bind(this);

    this.handle()
    window.addEventListener('resize', () => { this.handle() })
  }

  handle() {
    if (window.innerWidth <= 768 && this.isInit ) {
      this.reset();
      this.isInit = false;
    }

    if (window.innerWidth > 768 && !this.isInit) {
      this.isInit = true;
      this.placeEvents();
    }
  }

  reset() {
    this.singleEvents.forEach(event => {
      event.removeAttribute('style');
    })
  }

  placeEvents() {
    let slotHeight = this.firstHourElement.offsetHeight;
    this.singleEvents.forEach(event => {
      let start = this.getScheduleTimestamp(event.getAttribute('data-start'));
      let duration = this.getScheduleTimestamp(event.getAttribute('data-end')) - start;
      let eventTop = slotHeight * (start - this.timelineStart) / this.timelineUnitDuration;
      let eventHeight = slotHeight * duration / this.timelineUnitDuration;
      event.setAttribute('style', 'top: ' + (eventTop + this.hourAfterTop) + 'px; height: ' + (eventHeight - this.computedStyleTop - this.computedStyleBottom + this.hourAfterHeight) + 'px; position: absolute;');
    })
  }

  getScheduleTimestamp(time) {
    // Accepts hh:mm format - convert hh:mm to timestamp
    time = time.replace(/ /g, '');
    let timeArray = time.split(':');
    let timeStamp = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
    return timeStamp;
  }

}
