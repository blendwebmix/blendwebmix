/** import external dependencies */
import $ from 'jquery';

/** import local dependencies */
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import taxSessionType from './routes/taxSessionType';
import skipLinkFocus from './skip-link-focus-fix';
import navigation from './navigation';

navigation();
skipLinkFocus();

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
  common,
  home,
  taxSessionType,
});

function checkCSS() {
  const repeat = requestAnimationFrame(checkCSS);
  // CSS loaded?
  if ( getComputedStyle(document.body).boxSizing === 'border-box' ) {
    routes.loadEvents(); // Init JS
    cancelAnimationFrame(repeat); // Cancel next frame
  }
}

if (process.env.NODE_ENV !== 'production') {
  checkCSS();
} else {
  $(document).ready(() => {
    routes.loadEvents();
  });
}
