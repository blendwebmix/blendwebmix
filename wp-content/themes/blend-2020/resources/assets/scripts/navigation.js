var anchors = document.querySelectorAll('.menu-item-has-children > a');

for (var i = 0; i < anchors.length; i++) {
  anchors[i].addEventListener('click', function (e) {
    e.preventDefault();
    e.target.parentNode.classList.toggle('open')
  })
}


/**
 * a11y menu
 */

/* Menu button */
const menuButton = document.querySelector('.menu-toggle');

/* Toggle tab-index of every elements outside the menu */
const header = document.querySelector('.header');

/* Element that needs to be focusable */
const focusables = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';

/* Get focusable element from menu */
const headerFocusables = Array.prototype.slice.call(header.querySelectorAll(focusables));

/* Elements that needs to be focusables */
const headerFocusablesStay = Array.prototype.slice.call(header.querySelectorAll('.logo, .header__btn--sponsor, .header__btn--ticketing, .menu-toggle'));

/* Get focusable element from document */
const documentFocusables = Array.prototype.slice.call(document.querySelectorAll(focusables));

// Remove focus on the menu elements
headerFocusables.map(value => value.setAttribute('tabindex', '-1'));
headerFocusablesStay.map(value => value.removeAttribute('tabindex'));

function menuFocus() {
  if (header.classList.contains('header__toggled')) {
    documentFocusables.map(value => value.setAttribute('tabindex', '-1'));

    // Make first menu focusable
    headerFocusablesStay.map(value => value.setAttribute('tabindex', '0'));
    headerFocusables.map(value => value.setAttribute('tabindex', '0'));

  } else {
    // Make element of document focusable again
    documentFocusables.map(value => value.removeAttribute('tabindex'));

    // Remove focus on the menu elements
    headerFocusables.map(value => value.setAttribute('tabindex', '-1'));

    headerFocusablesStay.map(value => value.removeAttribute('tabindex'));
  }
}

/* Toggle tabindex for accessibility */
menuButton.addEventListener('click', function () {
  menuFocus();
});

/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
export default function () {
  var container, button, menu, links, i, len;

  container = document.querySelector('.header');
  if (!container) {
    return;
  }

  button = container.querySelector('.menu-toggle');
  if ('undefined' === typeof button) {
    return;
  }

  menu = container.getElementsByTagName('ul')[0];

  // Hide menu toggle button if menu is empty and return early.
  if ('undefined' === typeof menu) {
    button.style.display = 'none';
    return;
  }

  container.setAttribute('aria-expanded', 'false');

  button.onclick = function () {
    if (-1 !== container.className.indexOf('toggled')) {
      document.body.className = document.body.className.replace(' body__open', '');
      container.className = container.className.replace(' header__toggled', '');
      button.setAttribute('aria-expanded', 'false');
      container.setAttribute('aria-expanded', 'false');
    } else {
      document.body.className += ' body__open';
      container.className += ' header__toggled';
      button.setAttribute('aria-expanded', 'true');
      container.setAttribute('aria-expanded', 'true');
    }
  };

  // Get all the link elements within the menu.
  links = menu.getElementsByTagName('a');

  // Each time a menu link is focused or blurred, toggle focus.
  for (i = 0, len = links.length; i < len; i++) {
    links[i].addEventListener('focus', toggleFocus, true);
    links[i].addEventListener('blur', toggleFocus, true);
  }

  /**
   * Sets or removes .focus class on an element.
   */
  function toggleFocus() {
    var self = this;

    // Move up through the ancestors of the current link until we hit .nav-menu.
    while (-1 === self.className.indexOf('nav-menu')) {

      // On li elements toggle the class .focus.
      if ('li' === self.tagName.toLowerCase()) {
        if (-1 !== self.className.indexOf('focus')) {
          self.className = self.className.replace(' focus', '');
        } else {
          self.className += ' focus';
        }
      }

      self = self.parentElement;
    }
  }

  /**
   * Toggles `focus` class to allow submenu access on tablets.
   */
  (function (container) {
    var touchStartFn,
      i,
      parentLink = container.querySelectorAll('.menu-item-has-children > a, .page_item_has_children > a');

    if ('ontouchstart' in window) {
      touchStartFn = function (e) {
        var menuItem = this.parentNode, i;

        if (!menuItem.classList.contains('focus')) {
          e.preventDefault();
          for (i = 0; i < menuItem.parentNode.children.length; ++i) {
            if (menuItem === menuItem.parentNode.children[i]) {
              continue;
            }
            menuItem.parentNode.children[i].classList.remove('focus');
          }
          menuItem.classList.add('focus');
        } else {
          menuItem.classList.remove('focus');
        }
      };

      for (i = 0; i < parentLink.length; ++i) {
        parentLink[i].addEventListener('touchstart', touchStartFn, false);
      }
    }
  }(container));
}
