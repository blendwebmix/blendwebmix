<?php

namespace App;

/**
 * Crop & resize images
 * https://github.com/junaidbhura/fly-dynamic-image-resizer/wiki
 */

function resize($attachment_id, $size, $crop) {
  if(function_exists('fly_get_attachment_image_src')) {
    return fly_get_attachment_image_src($attachment_id, $size, $crop);
  }
}

/**
 * Don't index preprod sites
 */
add_action('wp_head', function () {
    if (strpos($_SERVER['SERVER_NAME'], 'dev') !== false) {
        wp_no_robots();
    }
});

add_action( 'pre_get_posts', function ( $query ) {
    $query->set( 'ignore_sticky_posts', true );
} );

/**
 * Styles & Scripts
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('google-fonts', '//fonts.googleapis.com/css?family=Material+Icons|Montserrat:400,500,700', false, null);
    wp_enqueue_script('polyfill/picture', '//cdn.polyfill.io/v2/polyfill.js', false, null);
});

/**
 * Remove WordPress Default Image Sizes
 * https://wpbeaches.com/remove-wordpress-default-image-sizes/
 */

add_filter('max_srcset_image_width', function(){
  return true;
});
//add_filter('intermediate_image_sizes_advanced', function ($sizes) {
////    unset($sizes['thumbnail']);
////    unset($sizes['medium']);
////    unset($sizes['medium-large']);
////    unset($sizes['large']);
//    return $sizes;
//});

/**
 * Add options page
 * https://www.advancedcustomfields.com/resources/options-page/
 */

//if (function_exists('acf_add_options_page')) {
//    acf_add_options_page();
//}

/**
 * Remove Emojis scripts & styles
 * https://korben.info/wordpress-comment-se-debarrasser-des-emojis.html
 */

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**
 * Remove dashboard boxes loading AJAX contents
 * https://premium.wpmudev.org/forums/topic/remove-wordpress-news-widget-from-dashboard#post-1087866
 */

// Remove default dashboard news widget
// this may work when you're trying to use wp_dashboard_setup and the dashboard_primary widget keeps showing up
add_action('admin_init', function () {
    remove_meta_box('dashboard_primary', 'dashboard', 'normal');
});

// Remove network dashboard news widget

add_action('wp_network_dashboard_setup', function () {
    remove_meta_box('dashboard_primary', 'dashboard-network', 'side');
});


/**
 * Fix ACF Google Map API Key
 * https://www.advancedcustomfields.com/resources/acf-fields-google_map-api/
 */

// add_filter('acf/fields/google_map/api', function( $api ){
//   $api['key'] = '';
//   return $api;
// });



