<?php

namespace App;

/**
 * Theme customizer
 */
add_action( 'customize_register', function ( \WP_Customize_Manager $wp_customize ) {
    // Add postMessage support
    $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial( 'blogname', [
        'selector'        => '.brand',
        'render_callback' => function () {
            bloginfo( 'name' );
        }
    ] );
} );

/**
 * Customizer JS
 */
add_action( 'customize_preview_init', function () {
    wp_enqueue_script( 'sage/customizer.js', asset_path( 'scripts/customizer.js' ), [ 'customize-preview' ], null, true );
} );


function bwm_block_render_callback( $block ) {

    // Convert name ("acf/testimonial") into path friendly slug ("testimonial")
    $slug = str_replace( 'acf/', '', $block['name'] );

    // Include a template part from within the "template-parts/block" folder
    if ( file_exists( locate_template( "blocks/{$slug}" ) ) ) {
        echo template( "/blocks/{$slug}" );
    }
}

add_action( 'acf/init', function () {

    // Check function exists
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Register a social block
    acf_register_block( [
        'name'            => 'social',
        'title'           => __( 'Bloc Social' ),
        'description'     => __( 'Bloc de liens sociaux.' ),
        'render_callback' => __NAMESPACE__ . '\\bwm_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => [ 'social', 'facebook', 'twitter', 'youtube', 'snapchat' ],
    ] );

    // Register a social block
    acf_register_block( [
        'name'            => 'push',
        'title'           => __( 'Bloc Push' ),
        'description'     => __( 'Bloc CTA + image.' ),
        'render_callback' => __NAMESPACE__ . '\\bwm_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => [ 'push', 'image', 'cta' ],
    ] );

    // Register a home title block
    acf_register_block( [
        'name'            => 'homepage_title',
        'title'           => __( "Bloc Titre page d'accueil" ),
        'description'     => __( "Bloc utilisé comme titre pour la page d'accueil" ),
        'render_callback' => __NAMESPACE__ . '\\bwm_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => [ 'accueil', 'home', 'title', 'titre' ],
    ] );

} );
