<?php

namespace App;

class Ajax {

  public function __construct() {
    add_action( 'wp_ajax_events_listing', [ $this, 'bwm_ajax_events_listing' ] );
    add_action( 'wp_ajax_nopriv_events_listing', [ $this, 'bwm_ajax_events_listing' ] );
  }

  public function bwm_ajax_events_listing() {
    // Get page with same slug that current taxonomy
    $tax_page = get_posts( [
      'name'        => $_POST['slug'],
      'post_type'   => 'page',
      'post_status' => 'publish',
      'numberposts' => 1,
    ] );

    // Get translation of this page
    if ( ! empty( $tax_page ) ) :
      $post_id = pll_get_post( $tax_page[0]->ID );
      $page    = get_post( $post_id );
    endif;

    // Get all sessions in this taxonomy, grouped by day
    $days          = [];
    $sessionsByDay = [];
    foreach ( get_terms( 'session_day', [ 'orderby' => 'slug' ] ) as $day ) {
      $days[]          = $day;
      $sessionsByDay[] = get_posts( [
        'post_type'      => 'session',
        'posts_per_page' => - 1,
        'meta_key'       => 'session_start',
        'orderby'        => 'meta_value',
        'order'          => 'ASC',
        'tax_query'      => [
          [
            'taxonomy' => 'session_day',
            'field'    => 'term_id',
            'terms'    => $day->term_id,
          ],
          [
            'taxonomy' => 'session_type',
            'field'    => 'term_id',
            'terms'    => $_POST['termId']
          ]
        ]
      ] );
    }

    $themes = get_terms( 'session_theme' );

    add_filter( 'sage/template/taxonomy-session_type/data', function ( array $data ) {
      $data['days']          = $days;
      $data['sessionsByDay'] = $sessionsByDay;
      $data['themes']        = $themes;

      return $data;
    } );

    echo \App\template( locate_template( 'views/partials/content-taxonomy-session_type-listing.blade.php' ) );
    wp_die();
  }
}

// new Ajax();
