<?php
/*
Plugin Name: Swapcard Connector
Plugin URI: https://www.blendwebmix.com/
Description: Syncronisation des programmes et des speakers avec Swapcard
Author: Yann Verneau
Version: 1.0
Author URI: https://www.blendwebmix.com/
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Definitions
 *
 * @since 1.0
 */
global $wpdb;
define('SWAPCARD_CONNECTOR_VERSION', '1.0');
define('SWAPCARD_CONNECTOR_PLUGIN_DIR', WP_PLUGIN_DIR.'/'.dirname(plugin_basename(__FILE__)));
define('SWAPCARD_CONNECTOR_PLUGIN_URL', WP_PLUGIN_URL.'/'.dirname(plugin_basename(__FILE__)));
define('SWAPCARD_CONNECTOR_PLUGIN_NAME', 'Swapcard Connector');
define('SWAPCARD_CONNECTOR_DEBUG', false);

define('SWAPCARD_CONNECTOR_API_URL', get_option('swapcard_connector_api_url', 'https://developer.swapcard.com/event-admin/graphql'));
define('SWAPCARD_CONNECTOR_API_ACCESS_TOKEN', get_option('swapcard_connector_api_access_token', 'NWM3ODFiYjRjYjg0ZjQwMDEwYjkzNjJjOmRmZTNmYjVlNmNjMjQ3OWE4ZDM2ZWE1OTI5NDI3YzQzj'));
define('SWAPCARD_CONNECTOR_API_EVENT_ID', get_option('swapcard_connector_api_event_id', 'RXZlbnRfOTU0MDA='));
define('SWAPCARD_CONNECTOR_API_SPEAKER_GROUP_ID', get_option('swapcard_connector_api_speaker_group_id', 'RXZlbnRHcm91cF8xNjc1Mg=='));


/**
 * Settings page
 *
 */
function swapcard_connector_register_settings() {

  register_setting( 'swapcard_connector_settings', 'swapcard_connector_api_url', array('default' => get_option('swapcard_connector_api_url')) );
  register_setting( 'swapcard_connector_settings', 'swapcard_connector_api_access_token', array('default' => get_option('swapcard_connector_api_access_token')) );
  register_setting( 'swapcard_connector_settings', 'swapcard_connector_api_event_id', array('default' => get_option('swapcard_connector_api_event_id')) );
  register_setting( 'swapcard_connector_settings', 'swapcard_connector_api_speaker_group_id', array('default' => get_option('swapcard_connector_api_speaker_group_id')) );


  add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'custom_add_plugin_page_settings_link');
  function custom_add_plugin_page_settings_link( $links ) {
  	$links[] = '<a href="' .
  		admin_url( 'options-general.php?page=swapcard_connector' ) .
  		'">' . __('Settings') . '</a>';
  	return $links;
  }

}

add_action( 'admin_init', 'swapcard_connector_register_settings' );

function swapcard_connector_register_options_page() {
  add_options_page('Swapcard Connector Réglages', 'Swapcard Connector', 'manage_options', 'swapcard_connector', 'swapcard_connector_options_page');
}
add_action('admin_menu', 'swapcard_connector_register_options_page');

function add_swapcard_connector_options_style() {
    wp_register_style('options_page_style', plugins_url('assets/css/style.css',__FILE__));
    wp_enqueue_style('options_page_style');
}
add_action( 'admin_menu', 'add_swapcard_connector_options_style' );

function swapcard_connector_options_page() {
    wp_register_style('options_page_style', plugins_url('assets/css/bootstrap.min.css',__FILE__));
    wp_enqueue_style('options_page_style');
    ?>
    <div class="page">
        <h1>Swapcard Connector</h1>
        <form method="post" action="options.php" class="form">
            <?php settings_fields( 'swapcard_connector_settings' ); ?>
            <?php do_settings_sections( 'swapcard_connector_settings' ); ?>
            <div class="section">
                <h2>Tokens API Swapcard</h2>
                <table>
                    <tr valign="top">
                        <th align="left" scope="row"><label for="swapcard_connector_api_url">API URL</label></th>
                        <td><input type="text" id="swapcard_connector_api_url" name="swapcard_connector_api_url" value="<?php echo get_option('swapcard_connector_api_url'); ?>" style="width: 600px;" /></td>
                    </tr>
                    <tr valign="top">
                        <th align="left" scope="row"><label for="swapcard_connector_api_access_token">API KEY</label></th>
                        <td><input type="text" id="swapcard_connector_api_access_token" name="swapcard_connector_api_access_token" value="<?php echo get_option('swapcard_connector_api_access_token'); ?>" style="width: 600px;" /></td>
                    </tr>
                    <tr valign="top">
                        <th align="left" scope="row"><label for="swapcard_connector_api_event_id">Event id</label></th>
                        <td><input type="text" id="swapcard_connector_api_event_id" name="swapcard_connector_api_event_id" value="<?php echo get_option('swapcard_connector_api_event_id'); ?>" style="width: 600px;" /></td>
                    </tr>
                    <tr valign="top">
                        <th align="left" scope="row"><label for="swapcard_connector_api_speaker_group_id">Speaker group id</label></th>
                        <td><input type="text" id="swapcard_connector_api_speaker_group_id" name="swapcard_connector_api_speaker_group_id" value="<?php echo get_option('swapcard_connector_api_speaker_group_id'); ?>" style="width: 600px;" /></td>
                    </tr>
                </table>
            </div>
    

            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}
