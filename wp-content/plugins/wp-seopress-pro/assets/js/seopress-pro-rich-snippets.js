//Schemas tabs
jQuery(document).ready(function($){
    if ($('#seopress-schemas-tabs').length) {
        $("#seopress-schemas-tabs .hidden").removeClass('hidden');
        $("#seopress-schemas-tabs").tabs();
    }

    //Schemas post type
    $('#seopress-your-schema select.dyn').change(function(e) {
        e.preventDefault();

        var select = $(this).val();

        if (select == 'manual_global') {
            $(this).next('input.manual_global').show();
            $(this).closest('p').find('input.manual_global').show();
            $(this).closest('p').find('select.cf').hide();
        } else if (select == 'manual_img_global') {
            $(this).next('input.manual_img_global').show();
            $(this).closest('p').find('input.manual_img_library_global').hide();
            $(this).closest('p').find('select.cf').hide();
        } else if (select == 'manual_img_library_global') {
            $(this).next('input.manual_img_global').hide();
            $(this).closest('p').find('input.manual_img_library_global').show();
            $(this).closest('p').find('select.cf').hide();
        } else if (select == 'manual_date_global') {
            $(this).next('input.manual_date_global').show();
            $(this).closest('p').find('select.cf').hide();
        } else if (select == 'manual_time_global') {
            $(this).next('input.manual_time_global').show();
            $(this).closest('p').find('select.cf').hide();
        } else if (select == 'manual_rating_global') {
            $(this).next('input.manual_rating_global').show();
            $(this).closest('p').find('select.cf').hide();
        } else if (select == 'custom_fields') {
            $(this).closest('p').find('input').hide();
            $(this).closest('p').find('input.manual_img_global').hide();
            $(this).closest('p').find('input.manual_img_library_global').hide();
            $(this).closest('p').find('input.manual_date_global').hide();
            $(this).closest('p').find('input.manual_time_global').hide();
            $(this).closest('p').find('input.manual_rating_global').hide();
            $(this).closest('p').find('select.cf').show();
        } else if (select == 'manual_custom_global') {
            $(this).closest('p').find('textarea.manual_custom_global').show();
            $(this).closest('p').find('select.cf').hide();
        } else {
            $(this).closest('p').find('select.cf').hide();
            $(this).closest('p').find('input').hide();
            $(this).closest('p').find('input').hide();
            $(this).closest('p').find('textarea').hide();
        }
    }).trigger('change');

    //Rich Snippets Select
	if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'none') {
        $('.wrap-rich-snippets-type .advice').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'articles') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-articles').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'localbusiness') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-local-business').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'faq') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-faq').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'courses') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-courses').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'recipes') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-recipes').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'videos') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-videos').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'events') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-events').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'products') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-products').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'services') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-services').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'review') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-review').show();
    } else if ($("#seopress_pro_rich_snippets_type option:selected").val() == 'custom') {
        $('.wrap-rich-snippets-type .advice').hide();
        $('.wrap-rich-snippets-custom').show();
    }
    
	$('#seopress_pro_rich_snippets_type').change(function() {
		var seopress_rs_type = $('#seopress_pro_rich_snippets_type').val();
	    if (seopress_rs_type == 'none') {
	    	$('.wrap-rich-snippets-type .advice').show();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }
	    if (seopress_rs_type == 'articles') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').show();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }	    
	    if (seopress_rs_type == 'localbusiness') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').show();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }
	    if (seopress_rs_type == 'faq') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').show();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }
	    if (seopress_rs_type == 'courses') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').show();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }
	    if (seopress_rs_type == 'recipes') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').show();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }
	    if (seopress_rs_type == 'videos') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').show();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }
	    if (seopress_rs_type == 'events') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').show();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }
	    if (seopress_rs_type == 'products') {
            $('.wrap-rich-snippets-type .advice').hide();
            $('.wrap-rich-snippets-articles').hide();
            $('.wrap-rich-snippets-local-business').hide();
            $('.wrap-rich-snippets-faq').hide();
            $('.wrap-rich-snippets-courses').hide();
            $('.wrap-rich-snippets-recipes').hide();
            $('.wrap-rich-snippets-videos').hide();
            $('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').show();
            $('.wrap-rich-snippets-services').hide();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
        }
        if (seopress_rs_type == 'services') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').show();
            $('.wrap-rich-snippets-review').hide();
            $('.wrap-rich-snippets-custom').hide();
	    }
        if (seopress_rs_type == 'review') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
	    	$('.wrap-rich-snippets-review').show();
	    	$('.wrap-rich-snippets-custom').hide();
        }
        if (seopress_rs_type == 'custom') {
	    	$('.wrap-rich-snippets-type .advice').hide();
	    	$('.wrap-rich-snippets-articles').hide();
	    	$('.wrap-rich-snippets-local-business').hide();
	    	$('.wrap-rich-snippets-faq').hide();
	    	$('.wrap-rich-snippets-courses').hide();
	    	$('.wrap-rich-snippets-recipes').hide();
	    	$('.wrap-rich-snippets-videos').hide();
	    	$('.wrap-rich-snippets-events').hide();
            $('.wrap-rich-snippets-products').hide();
            $('.wrap-rich-snippets-services').hide();
	    	$('.wrap-rich-snippets-review').hide();
	    	$('.wrap-rich-snippets-custom').show();
        }
	});

    //Rich Snippets Counters - Articles - Headline
	$("#seopress_rich_snippets_articles_counters").after("<div id=\"seopress_rich_snippets_articles_counters_val\">/ 110</div>");
    
    if ($("#seopress_rich_snippets_articles_counters").length != 0) {
        $("#seopress_rich_snippets_articles_counters").text($("#seopress_pro_rich_snippets_article_title").val().length);
    	if($('#seopress_pro_rich_snippets_article_title').val().length > 110){   
            $('#seopress_rich_snippets_articles_counters').css('color', 'red');
        }
        $("#seopress_pro_rich_snippets_article_title").keyup(function(event) {
        	$('#seopress_rich_snippets_articles_counters').css('color', 'inherit');
         	if($(this).val().length > 110){
                $('#seopress_rich_snippets_articles_counters').css('color', 'red');
            }
         	$("#seopress_rich_snippets_articles_counters").text($("#seopress_pro_rich_snippets_article_title").val().length);
         	if($(this).val().length > 0){
         		$(".snippet-title-custom").text(event.target.value);
                $(".snippet-title").css('display', 'none');
                $(".snippet-title-custom").css('display', 'block');
                $(".snippet-title-default").css('display', 'none');
         	} else if($(this).val().length == 0) {
         		$(".snippet-title-default").css('display', 'block');
                $(".snippet-title-custom").css('display', 'none');
                $(".snippet-title").css('display', 'none');
         	};
        });
    }

    //Rich Snippets Counters - Courses - Description
	$("#seopress_rich_snippets_courses_counters").after("<div id=\"seopress_rich_snippets_courses_counters_val\">/ 60</div>");
    
    if ($("#seopress_rich_snippets_courses_counters").length != 0) {
        $("#seopress_rich_snippets_courses_counters").text($("#seopress_pro_rich_snippets_courses_desc").val().length);
    	if($('#seopress_pro_rich_snippets_courses_desc').val().length > 60){   
            $('#seopress_rich_snippets_courses_counters').css('color', 'red');
        }
        $("#seopress_pro_rich_snippets_courses_desc").keyup(function(event) {
        	$('#seopress_rich_snippets_courses_counters').css('color', 'inherit');
         	if($(this).val().length > 60){
                $('#seopress_rich_snippets_courses_counters').css('color', 'red');
            }
         	$("#seopress_rich_snippets_courses_counters").text($("#seopress_pro_rich_snippets_courses_desc").val().length);
         	if($(this).val().length > 0){
         		$(".snippet-title-custom").text(event.target.value);
                $(".snippet-title").css('display', 'none');
                $(".snippet-title-custom").css('display', 'block');
                $(".snippet-title-default").css('display', 'none');
         	} else if($(this).val().length == 0) {
         		$(".snippet-title-default").css('display', 'block');
                $(".snippet-title-custom").css('display', 'none');
                $(".snippet-title").css('display', 'none');
         	};
        });
    }

    //Date picker
	$('.seopress-date-picker').datepicker({
        dateFormat: 'yy-mm-dd',
        beforeShow: function(input, inst) {
            $('#ui-datepicker-div').removeClass('ui-date-picker').addClass('seopress-ui-datepicker');
        }
	});

    //FAQ
    var template = $('#wrap-faq .faq:last').clone();

    //accordion
    var stop = false;
    $("#wrap-faq .faq h3").click(function(event) {
        if (stop) {
            event.stopImmediatePropagation();
            event.preventDefault();
            stop = false;
        }
    });
    function seopress_call_faq_accordion() {
        $( "#wrap-faq .faq" ).accordion({
            collapsible: true,
            active: false,
            heightStyle:"panel",
        });
    }
    seopress_call_faq_accordion();

    //define counter
    var sectionsCount = $('#wrap-faq').attr('data-count');

    //add new section
    $('#add-faq').click(function() {

        //increment
        sectionsCount++;

        //loop through each input
        var section = template.clone().find(':input').each(function(){
            //Stock input id
            var input_id = this.id;
            
            //Stock input name
            var input_name = this.name;

            //set id to store the updated section number
            var newId = this.id.replace(/^(\w+)\[.*?\]/, '$1['+sectionsCount+']');

            //Update input name
            $(this).attr('name', input_name.replace(/^(\w+)\[.*?\]/, '$1['+sectionsCount+']'));
            
            //update for label
            $(this).prev().attr('for', input_id.replace(/^(\w+)\[.*?\]/, '$1['+sectionsCount+']'));
            $(this).prev().attr('id', input_name.replace(/^(\w+)\[.*?\]/, '$1['+sectionsCount+']'));
            

            //update id
            this.id = newId;

        }).end()

        //inject new section
        .appendTo('#wrap-faq');
        seopress_call_faq_accordion();
        $( "#wrap-faq .faq" ).accordion('destroy');
        seopress_call_faq_accordion();
        
        return false;
    });

    //remove section
    $('#wrap-faq').on('click', '.remove-faq', function() {
        //fade out section
        $(this).fadeOut(300, function(){
            $(this).parent().parent().parent().parent().remove();
            return false;
        });
        return false;
    });
});